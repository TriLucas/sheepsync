/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.updates;

public class Version {
    private String mOrig;

    private long mMajor = 0;
    private long mMinor = 0;
    private long mBug = 0;
    private long mBuild = 0;

    public Version(String version) {
        mOrig = version;
        String[] parts = version.split("\\.");
        for (int i = 0; i < parts.length; i++) {
            switch (i) {
                case 0:
                    mMajor = Long.parseLong(parts[i]);
                    break;
                case 1:
                    mMinor = Long.parseLong(parts[i]);
                    break;
                case 2:
                    mBug = Long.parseLong(parts[i]);
                    break;
                case 3:
                    mBuild = Long.parseLong(parts[i]);
                    break;
                case 4:
                default:
                    break;
            }
        }
    }

    public int compareTo(Version b) {
        int res = 0;
        if ((res = (int) (mMajor - b.mMajor)) != 0) {
            return res;
        }
        if ((res = (int) (mMinor - b.mMinor)) != 0) {
            return res;
        }
        if ((res = (int) (mBug - b.mBug)) != 0) {
            return res;
        }
        if ((res = (int) (mBuild - b.mBuild)) != 0) {
            return res;
        }
        return 0;
    }

    @Override
    public String toString() {
        return mOrig;
    }
}
