/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.updates;

import com.tlucas.sheepsync.BuildConfig;

public class BaseUpdates {
    protected static final String GITLAB_RELEASE_JOB =
        "build:app:" + BuildConfig.BUILD_TYPE;
    protected static final String ROOT_URL =
        "https://gitlab.com/TriLucas/sheepsync/-/jobs/artifacts/" +
        BuildConfig.BRANCH_NAME;
    public static final String VERSION_URL =
        ROOT_URL + "/file/app/build/outputs/apk/" + BuildConfig.BUILD_TYPE +
        "/output.json?job=" + GITLAB_RELEASE_JOB;
    public static final String APK_URL =
        ROOT_URL + "/raw/app/build/outputs/apk/" + BuildConfig.BUILD_TYPE +
        "/%s?job=" + GITLAB_RELEASE_JOB;
}
