/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.os.AsyncTask;

import com.tlucas.sheepsync.media.MediaObject;

public class ThumbNailLoader extends AsyncTask<MediaObject, Integer, Bitmap> {
    private ContentResolver mResolver;
    private ThumbNailLoaded mListener;

    public ThumbNailLoader(ContentResolver resolver, ThumbNailLoaded listener) {
        mResolver = resolver;
        mListener = listener;
    }

    @Override
    protected Bitmap doInBackground(MediaObject... items) {
        if (isCancelled()) {
            return null;
        }
        return items[0].getThumbnail(mResolver);
    }

    @Override
    protected void onPostExecute(Bitmap thumbnail) {
        super.onPostExecute(thumbnail);
        if (isCancelled()) {
            return;
        }
        mListener.onThumbNailLoaded(thumbnail);
    }

    public interface ThumbNailLoaded {
        void onThumbNailLoaded(Bitmap thumbnail);
    }
}
