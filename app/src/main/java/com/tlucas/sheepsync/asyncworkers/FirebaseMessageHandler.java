/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tlucas.sheepsync.shared.SharedPrefsManager;
import com.tlucas.sheepsync.shared.SheepReporter;
import com.tlucas.sheepsync.shared.firestore.DocumentUpdater;
import com.tlucas.sheepsync.updates.Version;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.tlucas.sheepsync.BuildConfig.SUBSCRIPTION_SUFFIX;
import static com.tlucas.sheepsync.BuildConfig.VERSION_NAME;
import static com.tlucas.sheepsync.MainActivity.MainSettings.getAutoBoot;
import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public class FirebaseMessageHandler extends FirebaseMessagingService {
    public static final String TOPIC_NEW_VERSION = "update" + SUBSCRIPTION_SUFFIX;
    public static final String TOPIC_REMOTE_SPAWN = "spawnServices" + SUBSCRIPTION_SUFFIX;
    public static final String TOPIC_CONFIG_REQUEST = "requestConfig" + SUBSCRIPTION_SUFFIX;

    public static final List<String> ALL_TOPICS = new ArrayList<>(Arrays
        .asList(TOPIC_NEW_VERSION, TOPIC_REMOTE_SPAWN, TOPIC_CONFIG_REQUEST));

    public static class SubscriptionSettings extends SharedPrefsManager {
        private static final String TOPIC_SUBSCRIPTION_PREFIX = "subscribed_";

        private static String getFullPrefName(String topic) {
            if (!ALL_TOPICS.contains(topic)) {
                return null;
            } else {
                return TOPIC_SUBSCRIPTION_PREFIX + topic;
            }
        }

        static Boolean getTopicSubscription(Context ctx, String topic) {
            String pref = getFullPrefName(topic);
            return getSharedPrefs(ctx).getBoolean(pref, false);
        }

        static void putTopicSubscription(
            Context ctx, String topic, boolean val
        ) {
            String pref = getFullPrefName(topic);
            getSharedPrefsEditor(ctx).putBoolean(pref, val).apply();
        }
    }

    private String getTopic(RemoteMessage message) {
        String[] splitted = message.getFrom().split("/");
        return splitted[splitted.length - 1];
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage message) {
        String topic = getTopic(message);
        Map<String, String> data = message.getData();
        if (topic.equals(TOPIC_NEW_VERSION)) {
            String newVersion = data.get("new_version");
            if (newVersion != null && new Version(VERSION_NAME).compareTo(
                new Version(newVersion)) < 0) {
                SheepReporter.notifyUpdate(getApplicationContext());
            }
        } else if (topic.equals(TOPIC_REMOTE_SPAWN)) {
            boolean forceit = Boolean.parseBoolean(data.get("force"));
            if (forceit || getAutoBoot(getApplicationContext())) {
                MediaWatcherWorker.enqueueUnique(getApplicationContext());
            }
        } else if (topic.equals(TOPIC_CONFIG_REQUEST)) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user == null) {
                return;
            }
            String documentId = user.getEmail() + data.getOrDefault("uuid", "");

            DocumentUpdater update = new DocumentUpdater("configs", documentId);
            Map<String, ?> prefs = SharedPrefsManager.getAllPrefs(
                getApplicationContext());

            update.putAll(prefs);
            update.publish();
        }
    }

    public static void ensureSubscriptions(Context context) {
        for (String topic : ALL_TOPICS) {
            if (!SubscriptionSettings.getTopicSubscription(context, topic)) {
                FirebaseMessaging.getInstance().subscribeToTopic(topic)
                    .addOnCompleteListener((Task<Void> task) -> {
                        if (!task.isSuccessful()) {
                            SubscriptionSettings.putTopicSubscription(
                                context, topic, false);
                            fatalLog("Failed subscribing to" + topic, context);
                        } else {
                            SubscriptionSettings.putTopicSubscription(
                                context, topic, true);
                        }
                    });
            }
        }
    }
}
