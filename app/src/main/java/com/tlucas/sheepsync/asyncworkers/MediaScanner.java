/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import android.content.ContentResolver;
import android.content.Context;
import android.os.AsyncTask;

import com.tlucas.sheepsync.media.GroupedMediaList;
import com.tlucas.sheepsync.media.MediaQuery;
import com.tlucas.sheepsync.media.MediaViewArrayAdapter;
import com.tlucas.sheepsync.media.SyncDatabaseLocal;

public class MediaScanner extends AsyncTask<Void, Void, MediaViewArrayAdapter> {
    private ContentResolver mResolver;
    private ScannerCallback mListener;

    private SyncDatabaseLocal mLocalDatabase = null;
    private long mTstamp;
    private char mCmp;

    public MediaScanner(
        Context context, ScannerCallback listener, long tStamp, char cmp,
        boolean unsynced
    ) {
        mResolver = context.getContentResolver();
        mListener = listener;

        if (unsynced) {
            mLocalDatabase = SyncDatabaseLocal.getInstance(context);
        } else {
            mTstamp = tStamp;
            mCmp = cmp;
        }
    }

    @Override
    protected MediaViewArrayAdapter doInBackground(Void... param) {
        MediaQuery query = null;
        if (mLocalDatabase == null) {
            query = new MediaQuery(mTstamp, mCmp, mResolver);
        } else {
            query = new MediaQuery(mResolver, mLocalDatabase);
        }

        if (query.isValid()) {
            return new MediaViewArrayAdapter(
                new GroupedMediaList(query.execute()));
        }

        return null;
    }

    @Override
    protected void onPostExecute(MediaViewArrayAdapter media) {
        super.onPostExecute(media);
        mListener.onScanDone(media);
        if (mLocalDatabase != null) {
            mLocalDatabase.close();
            mLocalDatabase = null;
        }
    }

    public interface ScannerCallback {
        void onScanDone(MediaViewArrayAdapter media);
    }
}
