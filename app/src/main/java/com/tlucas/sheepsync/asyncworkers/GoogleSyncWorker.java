/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import android.content.ContentResolver;
import android.content.Context;
import android.os.AsyncTask;

import com.tlucas.sheepsync.GoogleSync;
import com.tlucas.sheepsync.media.MediaQuery;
import com.tlucas.sheepsync.media.SyncDatabaseLocal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static com.tlucas.sheepsync.MainActivity.newTimestamp;
import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public class GoogleSyncWorker extends AsyncTask<Integer, Void, Void>
    implements GoogleSync.GoogleUploadListener {
    private boolean mIsInitialized = false;
    private int mObjectsDone = 0;
    private int mObjectsExisted = 0;
    private long mBytesPushed = 0;
    private ContentResolver mResolver;
    private GoogleSync syncer;
    private SyncDatabaseLocal mLocalDb;
    private List<ProgressUpdate> mListeners;
    private long mStart;

    public GoogleSyncWorker(
        Context context, boolean doDelete
    ) {
        if (!(context instanceof ProgressUpdate)) {
            throw new RuntimeException(
                context.toString() + " must implement ProgressUpdate");
        }

        mLocalDb = SyncDatabaseLocal.getInstance(context);
        mResolver = context.getContentResolver();
        syncer = new GoogleSync(context, mLocalDb, doDelete);
        mListeners = new ArrayList<>();
        mListeners.add((ProgressUpdate) context);
    }

    public int addListener(ProgressUpdate listener) {
        mListeners.add(listener);
        if (mIsInitialized) {
            listener.setInitial(syncer.getPayload(), syncer.getTotalBytes());
            if (mObjectsDone > 0) {
                listener.onProgressUpdate(mObjectsDone, mObjectsExisted,
                    syncer.getPayload(), mBytesPushed, syncer.getTotalBytes()
                );
            }
        }
        return mListeners.size() - 1;
    }

    public void removeListener(int id) {
        mListeners.remove(id);
    }

    @Override
    protected Void doInBackground(Integer[] ids) {
        mStart = newTimestamp();
        syncer.prepareUpload(new MediaQuery(ids, mResolver));

        mIsInitialized = true;
        for (ProgressUpdate listener : mListeners) {
            listener.setInitial(syncer.getPayload(), syncer.getTotalBytes());
        }
        syncer.run(this);

        return null;
    }

    public void prepareVariables() {
        mObjectsDone = 0;
        mObjectsExisted = 0;
        mBytesPushed = 0;
    }

    public void incObjectsDone() {
        mObjectsDone++;
    }

    public void incObjectsSkipped() {
        mObjectsExisted++;
    }

    public void addBytesDone(long bytes) {
        mBytesPushed += bytes;
    }

    public void publish() {
        publishProgress();
    }

    public void handleException(Throwable t) {
        fatalLog(t);
    }

    @Override
    protected void onProgressUpdate(Void... progress) {
        for (ProgressUpdate listener : mListeners) {
            listener.onProgressUpdate(mObjectsDone, mObjectsExisted,
                syncer.getPayload(), mBytesPushed, syncer.getTotalBytes()
            );
        }
    }

    private void shutDown() {
        // Reverse the list so that we handle listeners in LIFO
        Collections.reverse(mListeners);
        Iterator<ProgressUpdate> itr = mListeners.iterator();
        while (itr.hasNext()) {
            itr.next().onSyncerDone(mObjectsDone, mObjectsExisted, mBytesPushed,
                newTimestamp() - mStart
            );
            itr.remove();
        }

        mLocalDb.close();
    }

    @Override
    protected void onPostExecute(Void param) {
        shutDown();
    }

    @Override
    protected void onCancelled(Void param) {
        shutDown();
    }

    @Override
    public void onCancelled() {
        shutDown();
    }

    public interface ProgressUpdate {
        void onSyncerDone(
            int pushed, int skipped, long bytesPushed, long millis
        );

        void setInitial(int payload, long bytesAll);

        void onProgressUpdate(
            int pushed, int skipped, int payload, long bytesPushed,
            long bytesAll
        );
    }

    public interface InterfaceProxy {
        int proxyAddListener(ProgressUpdate listener);

        void proxyRemoveListener(int id);
    }
}
