/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.IBinder;

import java.util.List;

import static com.tlucas.sheepsync.shared.SheepReporter.ID_SYNC;
import static com.tlucas.sheepsync.shared.SheepReporter.buildNotification;
import static com.tlucas.sheepsync.shared.SheepReporter.cancelNotification;
import static com.tlucas.sheepsync.shared.SheepReporter.doNotify;

public class GoogleSyncForegroundService extends Service
    implements GoogleSyncWorker.ProgressUpdate,
    GoogleSyncWorker.InterfaceProxy {
    private static boolean mRunning = false;
    private final IBinder binder = new LocalBinder();
    private GoogleSyncWorker mGoogleSyncTask = null;

    public class LocalBinder extends Binder {
        public GoogleSyncForegroundService getService() {
            return GoogleSyncForegroundService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public static class GoogleFGSConnection implements ServiceConnection {
        private ConnectionListener mConn;

        public GoogleFGSConnection(ConnectionListener handler) {
            mConn = handler;
        }

        @Override
        public void onServiceConnected(
            ComponentName className, IBinder service
        ) {
            GoogleSyncForegroundService.LocalBinder binder =
                (GoogleSyncForegroundService.LocalBinder) service;

            mConn.setService(binder.getService());
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mConn.clearService();
        }

        public interface ConnectionListener {
            void setService(GoogleSyncForegroundService service);

            void clearService();
        }
    }

    public static boolean isRunning() {
        return mRunning;
    }

    public void onProgressUpdate(
        int pushed, int skipped, int payload, long bytesPushed, long bytesAll
    ) {
        doNotify(
            getApplicationContext(), pushed, skipped, payload, bytesPushed,
            bytesAll
        );
    }

    public void onSyncerDone(
        int pushed, int skipped, long bytesPushed, long time
    ) {
        cleanUp();
        stopSelf();
    }

    public void setInitial(int payload, long bytesAll) {
        doNotify(getApplicationContext(), payload);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        mRunning = true;
        boolean doDelete = intent.getBooleanExtra("delete", false);

        List<Integer> mIds = intent.getIntegerArrayListExtra("ids");
        startForeground(ID_SYNC, buildNotification(getApplicationContext()));

        if (mGoogleSyncTask != null) {
            mGoogleSyncTask.cancel(true);
        }

        Integer[] data = new Integer[]{};
        data = mIds.toArray(data);

        mGoogleSyncTask = new GoogleSyncWorker(this, doDelete);
        mGoogleSyncTask.execute(data);

        return START_NOT_STICKY;
    }

    public int proxyAddListener(GoogleSyncWorker.ProgressUpdate listener) {
        return mGoogleSyncTask.addListener(listener);
    }

    public void proxyRemoveListener(int id) {
        mGoogleSyncTask.removeListener(id);
    }


    public void cleanUp() {
        cancelNotification(getApplicationContext());
        mRunning = false;
    }

    public void cancel() {
        mGoogleSyncTask.cancel(false);
        cleanUp();
        stopSelf();
    }
}
