/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;

public class ApkDownloader extends Downloader {
    public ApkDownloader(Downloader.DownloadListener listener, File cacheDir) {
        super(listener);
        mCache = cacheDir;
    }

    private File mCache;
    private File mOutpout = null;
    private FileOutputStream mOutputStream = null;
    private InputStream mStream = null;

    @Override
    protected void setStream(InputStream stream) {
        mStream = stream;
    }


    @Override
    protected int read() throws IOException {
        byte data[] = new byte[2048];
        int count;

        if (mOutpout == null) {
            mOutpout = File.createTempFile("SheepSync", "APK", mCache);
            mOutputStream = new FileOutputStream(mOutpout);
        }

        count = mStream.read(data);
        if (count > 0) {
            mOutputStream.write(data, 0, count);
        }

        return count;
    }

    @Override
    protected String[] getResult() {
        return new String[]{mOutpout.getAbsolutePath()};
    }

    @Override
    protected void closeStreams() throws IOException {
        mOutputStream.close();
        mStream.close();
    }
}
