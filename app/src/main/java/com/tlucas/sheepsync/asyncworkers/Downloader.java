/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import android.os.AsyncTask;

import java.io.InputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public abstract class Downloader extends AsyncTask<URL, String, String[]> {
    private DownloadListener mListener;

    protected abstract int read() throws IOException;

    protected abstract String[] getResult();

    protected abstract void setStream(InputStream stream);

    protected abstract void closeStreams() throws IOException;

    private Downloader() {
    }

    Downloader(DownloadListener listener) {
        if (listener instanceof DownloadListener) {
            mListener = listener;
        }
    }

    protected String[] doInBackground(URL... params) {
        return executeNow(params);
    }

    public String[] executeNow(URL... params) {
        int count = 0;
        HttpURLConnection connection = null;
        try {

            URL url = params[0];
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                return null;
            }

            InputStream stream = connection.getInputStream();
            setStream(stream);
            mListener.setExpectedContentLength(connection.getContentLength());

            int size;

            while ((size = read()) > 0 && !isCancelled()) {
                count += size;
                mListener.onUpdate(count);
            }

            closeStreams();
            return getResult();
        } catch (IOException e) {
            fatalLog(e);
            return null;
        }
    }

    @Override
    protected void onPostExecute(String... result) {
        if (!isCancelled()) {
            mListener.onDownload(result);
        }
    }

    public interface DownloadListener {
        void onDownload(String[] result);

        void setExpectedContentLength(int length);

        void onUpdate(int progress);
    }
}
