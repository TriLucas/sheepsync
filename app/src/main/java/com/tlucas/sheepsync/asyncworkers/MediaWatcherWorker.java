/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.Worker;
import androidx.work.WorkerParameters;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import com.tlucas.sheepsync.GoogleSync;

import com.tlucas.sheepsync.media.MediaQuery;
import com.tlucas.sheepsync.media.SyncDatabaseLocal;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.tlucas.sheepsync.BuildConfig.APPLICATION_ID;
import static com.tlucas.sheepsync.media.MediaQuery.OBSERVED_MEDIA_CONTENT_URIS;
import static com.tlucas.sheepsync.shared.Logging.fatalLog;

/*
 * Android generally requires annotated Services (a JobService in this case)
 * to have a default constructor. However, androidx.work.Worker doesn't even
 * ALLOW a default constructor. Hilarious, isn't it? Praise Java, suppress
 * this lint error.
 * */
@SuppressLint({"Instantiatable"})
public class MediaWatcherWorker extends Worker {
    private static final String WORKER_TAG =
        "com.tlucas.sheepsync.asyncworkers.MediaWatcherWorker";
    public static final String WORKER_STARTED_INTENT =
        APPLICATION_ID + "WORKER_STARTED";
    public static final String WORKER_DONE_INTENT =
        APPLICATION_ID + "BACKGROUND_WORKER_DONE";

    private Context currentContext;
    private WorkerParameters currentParams;

    public MediaWatcherWorker(
        @NonNull Context context, @NonNull WorkerParameters params
    ) {
        super(context, params);
        currentContext = context;
        currentParams = params;
    }

    private static Constraints getConstraints() {
        Constraints.Builder builder =
            new Constraints.Builder().setRequiredNetworkType(
                NetworkType.UNMETERED);
        for (Uri uri : OBSERVED_MEDIA_CONTENT_URIS) {
            builder.addContentUriTrigger(uri, true);
        }

        return builder.build();
    }

    private static PeriodicWorkRequest getPeriodicWorkRequest() {
        return new PeriodicWorkRequest.Builder(MediaWatcherWorker.class,
            Duration.ofMillis(PeriodicWorkRequest.MIN_PERIODIC_INTERVAL_MILLIS)
        ).setConstraints(getConstraints()).addTag(WORKER_TAG).build();
    }

    public static void enqueueUnique(Context context) {
        WorkManager.getInstance(context).enqueueUniquePeriodicWork(WORKER_TAG,
            ExistingPeriodicWorkPolicy.KEEP, getPeriodicWorkRequest()
        );
    }

    public static List<WorkInfo> getIsRunning(Context context)
        throws ExecutionException, InterruptedException {
        return WorkManager.getInstance(context).getWorkInfosByTag(WORKER_TAG)
            .get();
    }

    public static void cancelWorkers(Context context) {
        WorkManager.getInstance(context).cancelAllWorkByTag(WORKER_TAG);
    }

    @Override
    public @NonNull
    Result doWork() {
        SyncDatabaseLocal localDb = SyncDatabaseLocal.getInstance(currentContext);
        GoogleSync syncer = new GoogleSync(currentContext, localDb);
        syncer.prepareUpload(new MediaQuery(currentParams, currentContext));

        if (syncer.getPayload() <= 0) {
            return Result.success();
        }

        LocalBroadcastManager broadcaster = LocalBroadcastManager.getInstance(
            currentContext);

        broadcaster.sendBroadcast(new Intent(WORKER_STARTED_INTENT));
        try {
            syncer.run();
        } catch (Throwable t) {
            fatalLog(t, currentContext);
            throw new RuntimeException(t);
        }
        broadcaster.sendBroadcast(new Intent(WORKER_DONE_INTENT));
        localDb.close();
        return Result.success();
    }
}
