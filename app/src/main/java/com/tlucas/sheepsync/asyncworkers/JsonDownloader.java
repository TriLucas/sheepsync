/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.asyncworkers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public class JsonDownloader extends Downloader {
    public JsonDownloader(Downloader.DownloadListener listener) {
        super(listener);
    }

    private StringBuffer mCache = null;
    private BufferedReader mReader = null;
    private InputStream mStream = null;

    @Override
    protected void setStream(InputStream stream) {
        mReader = new BufferedReader(new InputStreamReader(stream));
        mStream = stream;
    }

    @Override
    protected int read() throws IOException {
        char[] buf = new char[1024];
        if (mCache == null) {
            mCache = new StringBuffer();
        }

        int size = mReader.read(buf);
        if (size > 0) {
            mCache.append(buf, 0, size);
        }

        return size;
    }

    @Override
    protected String[] getResult() {
        String[] res = null;
        String tmpStr = mCache.toString();
        try {
            JSONArray jArray = new JSONArray(mCache.toString());
            JSONObject tmp = jArray.getJSONObject(0).getJSONObject("apkData");
            String filename = tmp.getString("outputFile");
            String version = tmp.getString("versionName");
            res = new String[2];
            res[0] = version;
            res[1] = filename;

        } catch (JSONException e) {
            fatalLog(e);
        }
        return res;
    }

    @Override
    protected void closeStreams() throws IOException {
        mReader.close();
        mStream.close();
    }
}
