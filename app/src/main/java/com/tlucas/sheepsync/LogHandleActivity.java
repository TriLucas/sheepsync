/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.tlucas.sheepsync.shared.firestore.DocumentAdder;

import static com.tlucas.sheepsync.BuildConfig.VERSION_NAME;

public class LogHandleActivity extends Activity {
    private void firebaseLog(String stacktrace) {
        DocumentAdder doc = new DocumentAdder("logs");
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            doc.put("uuid", user.getUid());
            String email;
            if ((email = user.getEmail()) != null) {
                doc.put("email", email);
            }
        }
        doc.put("stacktrace", stacktrace);
        doc.put("version", VERSION_NAME);
        doc.publish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        firebaseLog(intent.getStringExtra("stack"));
        finish();
    }
}
