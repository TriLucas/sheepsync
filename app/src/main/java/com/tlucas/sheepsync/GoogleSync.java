/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync;

import android.content.ContentResolver;
import android.content.Context;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;

import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.tlucas.sheepsync.media.MediaObject;
import com.tlucas.sheepsync.media.MediaQuery;
import com.tlucas.sheepsync.media.SyncDatabaseLocal;

import java.io.IOException;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public class GoogleSync {
    public enum DriveResult {OK, EXISTS, ERROR}

    private static final String MIME_TYPE_FOLDER =
        "application/vnd.google-apps.folder";
    private static final String GDRIVE_SYNC_ROOT_FOLDER = "SheepSynced";

    private ContentResolver mResolver;
    private SyncDatabaseLocal mLocalDatabase;
    private final Drive mDriveService;

    private List<MediaObject> mObjects = null;
    private MediaObject mCurrent = null;
    private Iterator<MediaObject> mObjIter = null;

    private long mTotalBytes = 0;
    private Map<String, String> mBucketIds = new HashMap<>();

    private String ownerMail;
    private boolean mDelete;

    public GoogleSync(
        Context context, SyncDatabaseLocal localDb, boolean delete
    ) {
        mResolver = context.getContentResolver();
        mLocalDatabase = localDb;
        mDelete = delete;

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(
            context);

        GoogleAccountCredential credential =
            GoogleAccountCredential.usingOAuth2(context,
                Collections.singleton(DriveScopes.DRIVE_FILE)
            );
        credential.setSelectedAccount(account.getAccount());
        ownerMail = account.getEmail();

        mDriveService = new Drive.Builder(new NetHttpTransport(),
            new GsonFactory(), credential
        ).setApplicationName("SheepSync").build();
    }

    public GoogleSync(Context context, SyncDatabaseLocal localDb) {
        this(context, localDb, false);
    }

    private String getOrCreateFolder(String folder, String parentID) {
        try {
            FileList result = mDriveService.files().list().setQ(
                "mimeType = '" + MIME_TYPE_FOLDER + "' and name = '" + folder +
                "' ").setSpaces("drive").execute();

            if (result.getFiles().size() > 0) {
                for (int i = 0; i < result.size(); i++) {
                    File file = result.getFiles().get(i);
                    if (file.getName().equals(folder)) {
                        return file.getId();
                    }
                }
                return "root";
            } else {
                File metadata = new File().setParents(
                    Collections.singletonList(parentID)).setMimeType(
                    MIME_TYPE_FOLDER).setName(folder);

                File googleFile = mDriveService.files().create(metadata)
                    .execute();
                return googleFile.getId();
            }
        } catch (IOException e) {
            fatalLog(e);
        }
        return "root";
    }

    private String ensureFolder(String folder, String parentID) {
        if (folder.equals("root")) {
            return folder;
        }

        if (!mBucketIds.containsKey(folder)) {
            mBucketIds.put(folder, getOrCreateFolder(folder, parentID));
        }

        return mBucketIds.get(folder);
    }

    private boolean fileExists(MediaObject object) throws IOException {
        {
            String parent = ensureFolder(object.getBucket(),
                ensureFolder(GDRIVE_SYNC_ROOT_FOLDER, "root")
            );
            StringBuilder query = new StringBuilder();
            query.append("'").append(parent).append("' in parents ");
            query.append("and name = '").append(object.getFilename()).append(
                "' ");
            query.append("and createdTime = '").append(
                object.getGoogleDateTime()).append("' ");

            return mDriveService.files().list().setQ(query.toString())
                       .setFields("files(name, size, createdTime)").setSpaces(
                    "drive").execute().getFiles().size() > 0;
        }
    }

    public void prepareUpload(MediaQuery query) {
        if (query.isValid()) {
            mObjects = query.execute();
            mObjIter = mObjects.iterator();
        }
    }

    private DriveResult uploadFile(MediaObject object) throws IOException {
        if (fileExists(object)) {
            return DriveResult.EXISTS;
        }

        File metadata = new File().setParents(Collections.singletonList(
            ensureFolder(object.getBucket(),
                ensureFolder(GDRIVE_SYNC_ROOT_FOLDER, "root")
            ))).setMimeType(object.getMimeType()).setName(object.getFilename())
            .setCreatedTime(object.getGoogleDateTime());

        FileContent fileContent = new FileContent(
            object.getMimeType(), object.getFile());

        File fileMeta = mDriveService.files().create(metadata, fileContent)
            .execute();

        return fileMeta != null ? DriveResult.OK : DriveResult.ERROR;
    }

    public int getPayload() {
        if (mObjects != null) {
            return mObjects.size();
        } else {
            return 0;
        }
    }

    public long getTotalBytes() {
        if (mTotalBytes == 0) {
            if (mObjects != null) {
                for (MediaObject obj : mObjects) {
                    mTotalBytes += obj.getBytes();
                }
            }
        }
        return mTotalBytes;
    }

    private boolean hasNext() {
        return (mObjects != null && mObjIter != null && mObjIter.hasNext());
    }

    private long next() {
        mCurrent = mObjIter.next();
        return mCurrent.getBytes();
    }

    private DriveResult push() throws IOException {
        DriveResult r = uploadFile(mCurrent);

        if (r == DriveResult.OK || r == DriveResult.EXISTS) {
            mCurrent.markSynced(mLocalDatabase, ownerMail);
            if (mDelete) {
                mCurrent.delete(mResolver);
            }
        }

        if (r == DriveResult.EXISTS) {
            mTotalBytes -= mCurrent.getBytes();
        }

        return r;
    }

    public void run() throws Throwable {
        Throwable t = run(null);
        if (t != null) {
            throw t;
        }
    }

    public Throwable run(GoogleUploadListener listener) {
        boolean no_listener = listener == null;
        if (!no_listener) {
            listener.prepareVariables();
        }

        while (hasNext() && (no_listener || !listener.isCancelled())) {
            long bytes = next();
            GoogleSync.DriveResult result;
            try {
                if (((result = push()) != GoogleSync.DriveResult.ERROR) &&
                    !no_listener) {
                    if (result == GoogleSync.DriveResult.OK) {
                        listener.incObjectsDone();
                        listener.addBytesDone(bytes);
                    } else {
                        listener.incObjectsSkipped();
                    }
                    listener.publish();
                }
            } catch (Throwable t) {
                if (!no_listener) {
                    listener.handleException(t);
                } else {
                    return t;
                }
            }
        }
        return null;
    }

    public interface GoogleUploadListener {
        void prepareVariables();

        void incObjectsDone();

        void incObjectsSkipped();

        void addBytesDone(long bytes);

        void publish();

        boolean isCancelled();

        void handleException(Throwable t);
    }
}
