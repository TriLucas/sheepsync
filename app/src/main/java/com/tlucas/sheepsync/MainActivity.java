/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.tlucas.sheepsync.asyncworkers.GoogleSyncForegroundService;
import com.tlucas.sheepsync.asyncworkers.MediaWatcherWorker;
import com.tlucas.sheepsync.bcreceivers.MediaWorkerBroadcastReceiver;
import com.tlucas.sheepsync.fragments.AboutFragment;
import com.tlucas.sheepsync.fragments.interfaces.SwitchInterface;
import com.tlucas.sheepsync.fragments.MainFragment;
import com.tlucas.sheepsync.fragments.ManualSyncFragment;
import com.tlucas.sheepsync.fragments.ProgressFragment;
import com.tlucas.sheepsync.fragments.UpdateFragment;
import com.tlucas.sheepsync.shared.SharedPrefsManager;
import com.tlucas.sheepsync.shared.SheepReporter;

import java.io.File;
import java.sql.Timestamp;

import static com.tlucas.sheepsync.asyncworkers.FirebaseMessageHandler.ensureSubscriptions;
import static com.tlucas.sheepsync.fragments.UpdateFragment.DownloadedSettings.clearDownloadPath;
import static com.tlucas.sheepsync.fragments.UpdateFragment.DownloadedSettings.getDownloadPath;
import static com.tlucas.sheepsync.media.MediaQuery.QuerySettings.putNewestTimestamp;
import static com.tlucas.sheepsync.perms.GoogleLoginPermissions.initLogin;
import static com.tlucas.sheepsync.shared.Logging.fatalLog;
import static com.tlucas.sheepsync.shared.SharedPrefsManager.isFirstRun;
import static com.tlucas.sheepsync.shared.SheepReporter.createNotificationChannel;

public class MainActivity extends AppCompatActivity implements SwitchInterface,
    MediaWorkerBroadcastReceiver.MediaWorkUpdateInterface {

    public static long newTimestamp() {
        return new Timestamp(System.currentTimeMillis()).getTime() / 1000;
    }

    public static class MainSettings extends SharedPrefsManager {
        public static final String PREFS_AUTO_BOOT = "auto_boot";
        public static final String PREFS_DO_AUTO_ASK = "do_auto_ask";
        public static final String PREFS_VERSION_INSTALLED_AT =
            "version_installed";

        public static void putAutoAsk(Context context, boolean val) {
            getSharedPrefsEditor(context).putBoolean(PREFS_DO_AUTO_ASK, val)
                .apply();
        }

        public static boolean getAutoAsk(Context context) {
            return getSharedPrefs(context).getBoolean(PREFS_DO_AUTO_ASK, true);
        }

        public static void putAutoBoot(Context context, boolean status) {
            getSharedPrefsEditor(context).putBoolean(PREFS_AUTO_BOOT, status)
                .apply();
        }

        public static boolean getAutoBoot(Context context) {
            return getSharedPrefs(context).getBoolean(PREFS_AUTO_BOOT, false);
        }

        public static void putInstalledTstamp(Context context, long val) {
            getSharedPrefsEditor(context).putLong(
                PREFS_VERSION_INSTALLED_AT, val).apply();
        }

        public static long getInstalledTstamp(Context context) {
            return getSharedPrefs(context).getLong(
                PREFS_VERSION_INSTALLED_AT, 0);
        }
    }

    public static class SheepSyncExceptionHandler
        implements Thread.UncaughtExceptionHandler {
        Thread.UncaughtExceptionHandler mOriginal;
        Context mContext;

        public SheepSyncExceptionHandler(Context context) {
            mContext = context;
            mOriginal = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }

        public void uncaughtException(Thread thread, Throwable ex) {
            fatalLog(ex, mContext);
            mOriginal.uncaughtException(thread, ex);
        }
    }

    private void handleFirstRun() {
        if (!isFirstRun(getApplicationContext())) {
            return;
        }

        Context context = getApplicationContext();
        long tstamp = newTimestamp();

        MainSettings.putAutoAsk(context, true);
        MainSettings.putAutoBoot(context, true);
        MainSettings.putInstalledTstamp(context, tstamp);

        putNewestTimestamp(context, tstamp);

        MediaWatcherWorker.enqueueUnique(context);
    }

    private SheepSyncExceptionHandler mHandler;
    private MediaWorkerBroadcastReceiver mReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        mHandler = new SheepSyncExceptionHandler(getApplicationContext());

        handleFirstRun();

        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction =
            fragmentManager.beginTransaction();

        MainFragment fragment = new MainFragment();
        fragmentTransaction.add(R.id.FLcontainer, fragment);
        fragmentTransaction.commit();

        handleIntent(getIntent());
    }

    private void handleIntent(Intent intent) {
        if (intent.hasExtra("update")) {
            SheepReporter.cancelUpdateNotification(getApplicationContext());
            switchFragment(new UpdateFragment());
        } else if (GoogleSyncForegroundService.isRunning()) {
            switchFragment(new ManualSyncFragment());
            switchFragment(new ProgressFragment());
        }
    }

    public void switchFragment(Fragment next, boolean toBackStack) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.FLcontainer, next);

        if (toBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
    }

    public void switchFragment(Fragment next) {
        switchFragment(next, true);
    }

    public void popBackStack() {
        FragmentManager manager = getSupportFragmentManager();
        manager.popBackStack();
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_actions, menu);
        menu.findItem(R.id.CheckUpdate).setOnMenuItemClickListener(
            (MenuItem item) -> {
                switchFragment(new UpdateFragment());
                return true;
            });

        menu.findItem(R.id.About).setOnMenuItemClickListener(
            (MenuItem item) -> {
                switchFragment(new AboutFragment());
                return true;
            });

        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        createNotificationChannel(getApplicationContext());
        ensureSubscriptions(getApplicationContext());
        initLogin(this);
        clearDownload();
    }

    @Override
    public void onResume() {
        mReceiver = new MediaWorkerBroadcastReceiver(this);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mReceiver.unregister(this);
        mReceiver = null;
    }

    private void makeToast(int msg_id) {
        Toast toast = Toast.makeText(
            getApplicationContext(), msg_id, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    public void onStartWorker() { makeToast(R.string.msg_background_started); }

    public void onStopWorker() {
        makeToast(R.string.msg_background_stopped);
    }

    private void clearDownload() {
        Context context = getApplicationContext();
        String dl;
        if ((dl = getDownloadPath(context)) != null) {
            if (new File(dl).delete()) {
                clearDownloadPath(context);
            }
        }
    }
}
