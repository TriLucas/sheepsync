/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.shared;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import com.tlucas.sheepsync.MainActivity;
import com.tlucas.sheepsync.R;

import static com.tlucas.sheepsync.BuildConfig.APPLICATION_ID;

public class SheepReporter {
    public static final int ID_SYNC = 42;
    public static final int ID_UPDATE = 43;

    private static double bytesToMb(long bytes) {
        return (double) bytes / (1024.0 * 1024.0);
    }

    private static String getFileStringQuantity(Context ctx, int quantity) {
        return ctx.getResources().getQuantityString(
            R.plurals.base_n_files, quantity, quantity);
    }

    public static String buildPrepMsg(Context ctx, int payload) {
        return ctx.getString(
            R.string.msg_preparing_sync, getFileStringQuantity(ctx, payload));
    }

    public static String buildReport(
        Context ctx, int pushed, int skipped, int payload, long bytesPushed
    ) {
        String file_repr = getFileStringQuantity(ctx, pushed);
        String report;
        if (skipped == 0) {
            report = ctx.getString(R.string.msg_report_done_full_no_ex,
                bytesToMb(bytesPushed), file_repr
            );
        } else {
            report = ctx.getString(R.string.msg_report_done_full_ex,
                bytesToMb(bytesPushed), file_repr, skipped
            );
        }

        int diff = payload - (pushed + skipped);
        if (diff != 0) {
            report += "\n\n" + ctx.getResources().getQuantityString(
                R.plurals.msg_report_done_partial, diff,
                getFileStringQuantity(ctx, diff)
            );
        }
        return report;
    }

    public static String buildProgress(
        Context ctx, int pushed, int skipped, int payload, long bytesPushed,
        long bytesAll
    ) {
        String file_repr = getFileStringQuantity(ctx, payload);
        if (skipped == 0) {
            return ctx.getString(R.string.msg_sync_progress_no_ex, pushed,
                file_repr, bytesToMb(bytesPushed), bytesToMb(bytesAll)
            );
        } else {
            return ctx.getString(R.string.msg_sync_progress_ex, pushed,
                file_repr, skipped, bytesToMb(bytesPushed), bytesToMb(bytesAll)
            );
        }
    }

    private static NotificationManager getNotificationManager(Context ctx) {
        return ctx.getSystemService(NotificationManager.class);
    }

    public static void doNotify(
        Context ctx, int pushed, int skipped, int payload, long bytesPushed,
        long bytesAll
    ) {
        getNotificationManager(ctx).notify(ID_SYNC,
            buildNotification(ctx, true, pushed, skipped, payload, bytesPushed,
                bytesAll
            )
        );
    }

    public static void doNotify(Context ctx, int all) {
        getNotificationManager(ctx).notify(ID_SYNC,
            buildNotification(ctx, false, 0, 0, all, 0, 0)
        );
    }

    private static Notification buildUpdateNotification(Context ctx) {
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("update", true);

        PendingIntent pending = PendingIntent.getActivity(ctx, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        return new NotificationCompat.Builder(ctx, APPLICATION_ID)
            .setContentTitle(ctx.getString(R.string.app_name)).setSmallIcon(
                R.drawable.sheep_sync_foreground).setContentIntent(pending)
            .setContentText(ctx.getString(R.string.msg_update_available))
            .build();
    }

    public static void cancelUpdateNotification(Context ctx) {
        getNotificationManager(ctx).cancel(ID_UPDATE);
    }

    public static void notifyUpdate(Context ctx) {
        getNotificationManager(ctx).notify(
            ID_UPDATE, buildUpdateNotification(ctx));
    }

    public static void createNotificationChannel(Context ctx) {
        NotificationChannel channel = new NotificationChannel(APPLICATION_ID,
            ctx.getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW
        );
        channel.setShowBadge(false);
        getNotificationManager(ctx).createNotificationChannel(channel);
    }

    private static NotificationCompat.Builder getNotificationBuilder(
        Context ctx
    ) {
        Intent intent = new Intent(ctx, MainActivity.class);
        intent.setFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pending = PendingIntent.getActivity(ctx, 0, intent, 0);

        return new NotificationCompat.Builder(ctx, APPLICATION_ID)
            .setContentTitle(ctx.getString(R.string.app_name)).setSmallIcon(
                R.drawable.sheep_sync_foreground).setContentIntent(pending);
    }

    private static Notification buildNotification(
        Context ctx, boolean isProgress, int pushed, int skipped, int payload,
        long bytesPushed, long bytesAll
    ) {

        String msg;

        if (isProgress) {
            msg = buildProgress(
                ctx, pushed, skipped, payload, bytesPushed, bytesAll);
        } else {
            msg = buildPrepMsg(ctx, payload);
        }

        NotificationCompat.Builder builder = getNotificationBuilder(ctx)
            .setContentText(msg);
        if (isProgress) {
            builder.setProgress(payload, pushed, false);
        }
        return builder.build();
    }

    public static Notification buildNotification(Context ctx) {
        return getNotificationBuilder(ctx).setContentText(
            ctx.getString(R.string.app_hailer)).build();
    }

    public static void cancelNotification(Context ctx) {
        getNotificationManager(ctx).cancel(ID_SYNC);
    }
}
