/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.shared.firestore;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

public abstract class DocumentHandler {
    Map<String, Object> mData;
    private FirebaseFirestore mFirestoreInstance;
    private String mDatabaseName;

    DocumentHandler(String databasename) {
        mFirestoreInstance = FirebaseFirestore.getInstance();
        mDatabaseName = databasename;
        mData = new HashMap<>();
        mData.put("time", new Timestamp(System.currentTimeMillis()).getTime());

    }

    CollectionReference getCollectionReference() {
        return mFirestoreInstance.collection(mDatabaseName);
    }

    public void putAll(Map<String, ?> data) {
        mData.putAll(data);
    }

    public void put(String name, Object value) {
        mData.put(name, value);
    }

    public abstract void publish();
}
