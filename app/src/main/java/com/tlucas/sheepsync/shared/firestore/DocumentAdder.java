/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.shared.firestore;

public class DocumentAdder extends DocumentHandler {
    public DocumentAdder(String database) {
        super(database);
    }

    public void publish() {
        getCollectionReference().add(mData);
    }
}
