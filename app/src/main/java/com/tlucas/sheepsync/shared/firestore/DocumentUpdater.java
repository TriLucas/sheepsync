/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.shared.firestore;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;

public class DocumentUpdater extends DocumentHandler {
    private DocumentReference mDocument;

    public DocumentUpdater(String databasename, String documentid) {
        super(databasename);
        mDocument = getCollectionReference().document(documentid);
    }

    public void publish() {
        mDocument.get().addOnCompleteListener((Task<DocumentSnapshot> task) -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document != null && document.exists()) {
                    mDocument.update(mData);
                } else {
                    mDocument.set(mData);
                }
            }
        });
    }
}
