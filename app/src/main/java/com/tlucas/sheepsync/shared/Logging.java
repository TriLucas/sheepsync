/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.shared;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.tlucas.sheepsync.BuildConfig;
import com.tlucas.sheepsync.LogHandleActivity;

import java.io.PrintWriter;
import java.io.StringWriter;

public class Logging {
    private static final String LOGTAG = "SheepSync-Log:";

    private static void stacktraceLog(String stacktrace) {
        debugLog(Log.ERROR, stacktrace);
    }

    public static String fatalLog(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        t.printStackTrace(pw);

        String stacktrace = sw.toString();

        stacktraceLog(stacktrace);
        return stacktrace;
    }

    public static void fatalLog(String msg, Context context) {
        final int flags =
            Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME |
            Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS |
            Intent.FLAG_ACTIVITY_NO_HISTORY;

        final Intent activityIntent = new Intent(
            context, LogHandleActivity.class);
        activityIntent.setFlags(flags);
        activityIntent.putExtra("stack", msg);
        context.startActivity(activityIntent);
    }

    public static void fatalLog(Throwable t, Context context) {
        String stacktrace = fatalLog(t);
        fatalLog(stacktrace, context);
    }

    public static void debugLog(int priority, String msg) {
        if (BuildConfig.LOG_DEBUG_MODE) {
            Log.println(priority, LOGTAG, msg);
        }
    }

    public static void debugLog(String msg) {
        debugLog(Log.DEBUG, msg);
    }
}
