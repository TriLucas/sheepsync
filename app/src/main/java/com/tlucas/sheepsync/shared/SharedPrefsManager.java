/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.shared;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

import static com.tlucas.sheepsync.BuildConfig.APPLICATION_ID;
import static com.tlucas.sheepsync.MainActivity.MainSettings.PREFS_DO_AUTO_ASK;
import static com.tlucas.sheepsync.MainActivity.MainSettings.PREFS_VERSION_INSTALLED_AT;

public class SharedPrefsManager {
    private static final String PREF_FILE = APPLICATION_ID + ".PERSISTENT_DATA";

    private static final String[] FIRST_RUN_SATISFIERS =
        new String[]{PREFS_DO_AUTO_ASK, PREFS_VERSION_INSTALLED_AT};

    protected static SharedPreferences getSharedPrefs(Context context) {
        return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
    }

    protected static SharedPreferences.Editor getSharedPrefsEditor(
        Context context
    ) {
        return getSharedPrefs(context).edit();
    }

    public static Map<String, ?> getAllPrefs(Context context) {
        return getSharedPrefs(context).getAll();
    }

    public static boolean isFirstRun(Context context) {
        SharedPreferences prefs = getSharedPrefs(context);
        for (String opt : FIRST_RUN_SATISFIERS) {
            if (!(prefs.contains(opt))) {
                return true;
            }
        }
        return false;
    }
}
