/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.perms;

import android.content.pm.PackageManager;
import android.Manifest;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.tlucas.sheepsync.perms.base.PermissionSetObject;

public class BasePermissions extends PermissionSetObject {
    public int getCode() {
        return 1;
    }

    public static final String[] EXPLICIT_PERMISSIONS =
        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public BasePermissions(Fragment fragment) {
        super(fragment);
    }

    @Override
    public boolean isGranted() {
        for (String perm : EXPLICIT_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(mFragment.getContext(),
                perm
            ) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void requestGrant() {
        mFragment.requestPermissions(EXPLICIT_PERMISSIONS, getCode());
    }
}
