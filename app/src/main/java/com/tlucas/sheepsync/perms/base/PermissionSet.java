/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.perms.base;

public interface PermissionSet {
    boolean isGranted();
    void requestGrant();
}
