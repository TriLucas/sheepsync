/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.perms.base;

import android.content.Intent;

public interface ActivityResultCallback {
    public void activityCallback(int resultCode, Intent data);
}
