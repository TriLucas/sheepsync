/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.perms;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.tlucas.sheepsync.perms.base.PermissionSetObject;
import com.tlucas.sheepsync.R;

import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public class GoogleLoginPermissions extends PermissionSetObject {
    public int getCode() {
        return 2;
    }

    public GoogleLoginPermissions(Fragment fragment) {
        super(fragment);
    }

    public static void initLogin(Context context) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(
            context);

        if (account != null) {
            firebaseAuthWithGoogle(account);
        }
    }

    @Override
    public boolean isGranted() {
        return GoogleSignIn.getLastSignedInAccount(mFragment.getContext()) !=
               null;
    }

    @Override
    public void requestGrant() {
        GoogleSignInOptions signinOptions = new GoogleSignInOptions.Builder(
            GoogleSignInOptions.DEFAULT_SIGN_IN).requestScopes(
            new Scope(Scopes.DRIVE_FILE), new Scope(Scopes.DRIVE_APPFOLDER))
            .requestIdToken(mFragment.getContext()
                .getString(R.string.default_web_client_id)).requestEmail()
            .build();
        mFragment.startActivityForResult(
            GoogleSignIn.getClient(mFragment.getContext(), signinOptions)
                .getSignInIntent(), getCode());
    }

    @Override
    public void activityCallback(int resultCode, Intent data) {
        Task<GoogleSignInAccount> task =
            GoogleSignIn.getSignedInAccountFromIntent(data);
        handleSignInResult(task);
    }

    private static Task<AuthResult> firebaseAuthWithGoogle(
        GoogleSignInAccount acct
    ) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        AuthCredential credential = GoogleAuthProvider.getCredential(
            acct.getIdToken(), null);
        return auth.signInWithCredential(credential);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(
                ApiException.class);
            firebaseAuthWithGoogle(account);
        } catch (ApiException e) {
            fatalLog(e);
        }
    }
}
