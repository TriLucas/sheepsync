/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.perms.base;

import android.content.Intent;

import androidx.fragment.app.Fragment;

public abstract class PermissionSetObject
    implements PermissionSet, ActivityResultCallback,
    PermissionsRequestCallback {
    protected Fragment mFragment;

    public PermissionSetObject(Fragment fragment) {
        mFragment = fragment;
    }

    @Override
    public void permissionsCallback(String[] permissions, int[] grantResults) {
    }

    @Override
    public void activityCallback(int resultCode, Intent data) {
    }

    public abstract int getCode();
}
