/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.media;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

public class GroupedMediaList {
    public static final int VIEW_TYPE_OBJECT = 0;
    public static final int VIEW_TYPE_GROUP = 1;
    public static final int VIEW_TYPE_ERROR = -1;

    private SortedMap<String, List<MediaObject>> mGroupMedia = null;
    private List<String> mHiddenGroups;

    public GroupedMediaList(List<MediaObject> data) {
        handleRawData(data);
        mHiddenGroups = new ArrayList<>();
    }

    private void handleRawData(List<MediaObject> data) {
        mGroupMedia = new TreeMap<>();

        for (int i = 0; i < data.size(); i++) {
            String bucket = data.get(i).getBucket();
            if (!mGroupMedia.containsKey(bucket)) {
                mGroupMedia.put(bucket, new ArrayList<>());
            }

            mGroupMedia.get(bucket).add(data.get(i));
        }
    }

    public int typeAtPosition(int position) {
        int checked = 0;

        for (String group : mGroupMedia.keySet()) {
            int currentChunk = shownGroupSize(group);
            if (position < currentChunk + checked) {
                if (position == checked) {
                    return VIEW_TYPE_GROUP;
                }
                return VIEW_TYPE_OBJECT;
            }

            checked += currentChunk;
        }

        return VIEW_TYPE_ERROR;
    }

    public Object objectAtPosition(int position) {
        int checked = 0;

        for (String group : mGroupMedia.keySet()) {
            int currentChunk = shownGroupSize(group);
            if (position < currentChunk + checked) {
                if (position == checked) {
                    return group;
                }
                return mGroupMedia.get(group).get((position - checked - 1));
            }

            checked += currentChunk;
        }

        return null;
    }

    public int getDisplayedCount() {
        int sum = 0;
        for (String group : mGroupMedia.keySet()) {
            sum += shownGroupSize(group);
        }
        return sum;
    }

    public int getActualCount() {
        int sum = 0;
        for (List<MediaObject> list : mGroupMedia.values()) {
            sum += list.size();
        }
        return sum;
    }

    public int[] remove(String group) {
        int[] res = new int[]{groupPosition(group), shownGroupSize(group)};
        mGroupMedia.remove(group);
        return res;
    }

    public int[] remove(int id) {
        for (String group : mGroupMedia.keySet()) {
            List<MediaObject> list = mGroupMedia.get(group);
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getId() == id) {
                    if (groupItems(group) == 1) {
                        return remove(group);
                    }
                    list.remove(i);
                    return new int[]{groupPosition(group) + 1 + i, 1};
                }
            }
        }

        return new int[]{-1,-1};
    }

    public List<MediaObject> getRawData() {
        List<MediaObject> result = new ArrayList<>();
        for (List<MediaObject> list : mGroupMedia.values()) {
            result.addAll(list);
        }
        return result;
    }

    public boolean toggleHidden(String group) {
        if (!mHiddenGroups.contains(group)) {
            mHiddenGroups.add(group);
            return false;
        } else {
            mHiddenGroups.remove(group);
            return true;
        }
    }

    private int shownGroupSize(String group) {
        if (mHiddenGroups.contains(group)) {
            return 1;
        }
        return mGroupMedia.get(group).size() + 1;
    }

    public int groupPosition(String group) {
        int innerGroupPos = 0;
        int groupPos = 0;
        List<String> allGroups = new ArrayList<>(mGroupMedia.keySet());

        if ((innerGroupPos = allGroups.indexOf(group)) == 0) {
            return 0;
        }

        for (int i = 0; i < innerGroupPos; i++) {
            groupPos += shownGroupSize(group);
        }

        return groupPos;
    }

    public int groupItems(String group) {
        return mGroupMedia.get(group).size();
    }
}
