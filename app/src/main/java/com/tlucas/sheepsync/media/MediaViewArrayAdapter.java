/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.media;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tlucas.sheepsync.R;
import com.tlucas.sheepsync.media.views.BaseViewHolder;
import com.tlucas.sheepsync.media.views.GalleryGroupHolder;
import com.tlucas.sheepsync.media.views.MediaViewHolder;

import static com.tlucas.sheepsync.media.GroupedMediaList.VIEW_TYPE_OBJECT;

public class MediaViewArrayAdapter
    extends RecyclerView.Adapter<BaseViewHolder> {
    public GroupedMediaList objects;

    private void updateRemoval(int[] result) {
        int pos = result[0];
        int amount = result[1];
        notifyItemRangeRemoved(pos, amount);
    }

    public void remove(int id) {
        updateRemoval(objects.remove(id));
    }

    public void remove(String group) {
        updateRemoval(objects.remove(group));
    }

    public MediaViewArrayAdapter(GroupedMediaList data) {
        objects = data;
    }

    @Override
    public int getItemViewType(int position) {
        return objects.typeAtPosition(position);
    }

    @Override
    @NonNull
    public BaseViewHolder onCreateViewHolder(
        @NonNull ViewGroup parent, int viewType
    ) {
        if (viewType == VIEW_TYPE_OBJECT) {
            return new MediaViewHolder(parent, this, R.layout.image_thumb);
        } else {
            return new GalleryGroupHolder(parent, this, R.layout.image_grouper);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.setContent(position);
    }

    @Override
    public int getItemCount() {
        return objects.getDisplayedCount();
    }

    public void hideGroup(String group) {
        if (objects.toggleHidden(group)) {
            notifyItemRangeInserted(
                objects.groupPosition(group) + 1, objects.groupItems(group));

        } else {
            notifyItemRangeRemoved(
                objects.groupPosition(group) + 1, objects.groupItems(group));
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView parent) {
        GridLayoutManager manager = new GridLayoutManager(
            parent.getContext(), 4);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (getItemViewType(position) == VIEW_TYPE_OBJECT) {
                    return 1;
                }
                return 4;
            }
        });
        parent.setLayoutManager(manager);
        super.onAttachedToRecyclerView(parent);
    }
}
