/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.media;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.work.WorkerParameters;

import com.tlucas.sheepsync.shared.SharedPrefsManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.tlucas.sheepsync.MainActivity.newTimestamp;
import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public class MediaQuery {
    public static final Uri[] OBSERVED_MEDIA_CONTENT_URIS =
        new Uri[]{MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI};

    public static class QuerySettings extends SharedPrefsManager {
        public static final String PREFS_LAST_SERVICE_QUERIED =
            "last_service_queried";

        public static void putNewestTimestamp(Context context, long val) {
            getSharedPrefsEditor(context).putLong(
                PREFS_LAST_SERVICE_QUERIED, val).apply();
        }

        public static long getNewestTimestamp(Context context) {
            return getSharedPrefs(context).getLong(
                PREFS_LAST_SERVICE_QUERIED, -1);
        }

        static long swapNewestTimestamp(Context context) {
            long val = QuerySettings.getNewestTimestamp(context);
            putNewestTimestamp(context, newTimestamp());
            return val;
        }
    }

    private List<MediaObject> mObjects = new ArrayList<>();

    private String mQuery;

    private boolean isValidQuery = true;

    private ContentResolver mResolver;

    private SyncDatabaseLocal mLocalDatabase;

    public MediaQuery(ContentResolver resolver) {
        mResolver = resolver;
    }

    public MediaQuery(
        ContentResolver resolver, SyncDatabaseLocal localDatabase
    ) {
        mResolver = resolver;
        mLocalDatabase = localDatabase;

        queryUnSynced();
    }

    public MediaQuery(Long tStamp, char cmp, ContentResolver resolver) {
        this(resolver);

        // Must query </> tStamp ONLY
        if (tStamp != -1) {
            mQuery = tstampQuery(cmp, tStamp);
        } else {
            isValidQuery = false;
        }
    }

    public MediaQuery(Integer[] ids, ContentResolver resolver) {
        this(resolver);

        // Must query ids ONLY
        if (ids != null && ids.length > 0) {
            mQuery = idsInQuery(ids);
        } else {
            isValidQuery = false;
        }
    }

    public MediaQuery(WorkerParameters parameters, Context context) {
        this(context.getContentResolver());

        long tStamp = QuerySettings.swapNewestTimestamp(context);
        Integer[] ids = parseWorkerParams(parameters);

        // Must query ids AND > tStamp
        if (ids != null && ids.length > 0 && tStamp != -1) {
            mQuery = idsInQuery(ids) + " AND " + tstampQuery('>', tStamp);
        } else {
            isValidQuery = false;
        }
    }

    private String idsInQuery(Integer[] ids) {
        StringBuilder query = new StringBuilder();
        query.append(MediaObject.ID_COLUMN).append(" IN (");
        for (int i = 0; i < ids.length; i++) {
            if (i > 0) {
                query.append(", ");
            }
            query.append("'");
            query.append(ids[i]);
            query.append("'");
        }
        query.append(")");
        return query.toString();
    }

    private String tstampQuery(char cmp, long tStamp) {
        return MediaObject.CREATED_COLUMN + " " + cmp + " " + tStamp;
    }

    private Integer[] parseWorkerParams(WorkerParameters parameters) {
        HashSet<Integer> tmp = new HashSet<>();
        List<Uri> triggered = parameters.getTriggeredContentUris();

        for (Uri uri : triggered) {
            String auth = uri.getAuthority();
            if (auth != null && auth.equals("media")) {
                try {
                    String segment = uri.getLastPathSegment();
                    if (segment != null) {
                        tmp.add(Integer.parseInt(segment));
                    }
                } catch (NumberFormatException e) {
                    if (triggered.size() == 1) {
                        return null;
                    }
                }
            }
        }

        if (tmp.size() != 0) {
            Integer[] ids = new Integer[]{};
            ids = tmp.toArray(ids);
            return ids;
        }
        return null;
    }

    public boolean isValid() {
        return isValidQuery;
    }

    private void queryUnSynced() {
        Integer[] unsynced = mLocalDatabase.getAllNotSyncedIds(getAllIds());
        StringBuilder query = new StringBuilder();
        query.append(MediaObject.ID_COLUMN + " IN (");
        for (int i = 0; i < unsynced.length; i++) {
            if (i > 0) {
                query.append(", ");
            }
            query.append(unsynced[i]);
        }
        query.append(")");
        mQuery = query.toString();
    }

    private Integer[] getAllIds() {
        List<Integer> tmp = new ArrayList<>();
        for (Uri uri : OBSERVED_MEDIA_CONTENT_URIS) {
            Cursor cursor = null;
            try {
                cursor = mResolver.query(uri, MediaObject.PROJECTION, null,
                    null, MediaObject.getSortValue()
                );
                while (cursor.moveToNext()) {
                    tmp.add(cursor
                        .getInt(cursor.getColumnIndex(MediaObject.ID_COLUMN)));
                }
            } catch (Exception e) {
                fatalLog(e);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        Integer[] result = new Integer[]{};
        result = tmp.toArray(result);
        return result;
    }

    public List<MediaObject> execute() {
        if (!isValidQuery) {
            return null;
        }
        for (Uri uri : OBSERVED_MEDIA_CONTENT_URIS) {
            Cursor cursor = null;
            try {
                cursor = mResolver.query(uri, MediaObject.PROJECTION, mQuery,
                    null, MediaObject.getSortValue()
                );
                while (cursor.moveToNext()) {
                    mObjects.add(new MediaObject(cursor, uri));
                }
            } catch (Exception e) {
                fatalLog(e);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        return mObjects;
    }
}
