/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.media.views;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.tlucas.sheepsync.media.MediaViewArrayAdapter;

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    protected MediaViewArrayAdapter mAdapter;

    public BaseViewHolder(
        ViewGroup parent, MediaViewArrayAdapter parentAdapter, int layout_id
    ) {
        super(LayoutInflater.from(parent.getContext())
            .inflate(layout_id, parent, false));
        mAdapter = parentAdapter;
    }

    public abstract void setContent(int position);
}
