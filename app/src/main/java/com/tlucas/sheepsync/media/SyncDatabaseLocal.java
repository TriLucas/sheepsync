/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.media;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.Arrays;

public class SyncDatabaseLocal implements BaseColumns {
    private static final String TABLE_NAME = "sheepsynced_media";
    private static final String CNAME_PATH = "path";
    private static final String CNAME_ANDROID_ID = "android_id";
    private static final String CNAME_SYNCED_ON = "synced_on";
    private static final String CNAME_GOOGLE_ACCOUNT = "g_account";

    public static final String CREATE_TABLE_QUERY =
        "CREATE TABLE " + TABLE_NAME + " (" + _ID + " INTEGER PRIMARY KEY," +
        CNAME_PATH + " TEXT," + CNAME_ANDROID_ID + " INTEGER," +
        CNAME_SYNCED_ON + " TEXT," + CNAME_GOOGLE_ACCOUNT + " TEXT)";

    private SQLiteDatabase mDb;

    private SyncDatabaseLocal(SQLiteHelper dbHelper) {
        mDb = dbHelper.getWritableDatabase();
    }

    public static SyncDatabaseLocal getInstance(Context context) {
        return new SyncDatabaseLocal(new SQLiteHelper(context));
    }

    public void addEntry(String path, int id, long tstamp, String g_account) {
        ContentValues vals = new ContentValues();
        vals.put(CNAME_PATH, path);
        vals.put(CNAME_ANDROID_ID, id);
        vals.put(CNAME_SYNCED_ON, Long.toString(tstamp));
        vals.put(CNAME_GOOGLE_ACCOUNT, g_account);

        mDb.insert(TABLE_NAME, null, vals);
    }

    public Integer[] getAllNotSyncedIds(Integer[] available) {
        ArrayList<Integer> tmp = new ArrayList<>(Arrays.asList(available));
        String[] projection = {CNAME_ANDROID_ID};

        StringBuilder query = new StringBuilder();
        query.append(CNAME_ANDROID_ID + " IN (");
        for (int i = 0; i < available.length; i++) {
            if (i > 0) {
                query.append(", ");
            }
            query.append(available[i]);
        }
        query.append(")");

        Cursor cursor = mDb.query(TABLE_NAME, projection, query.toString(),
            null, null, null, null
        );

        while (cursor.moveToNext()) {
            tmp.remove(tmp.indexOf(
                cursor.getInt(cursor.getColumnIndex(CNAME_ANDROID_ID))));
        }
        cursor.close();

        Integer[] result = new Integer[]{};
        result = tmp.toArray(result);
        return result;
    }

    public void close() {
        mDb.close();
    }

    public static class SQLiteHelper extends SQLiteOpenHelper {
        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "sheepsynced.db";

        public SQLiteHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_QUERY);
        }

        public void onUpgrade(
            SQLiteDatabase db, int oldVersion, int newVersion
        ) {
        }

        public void onDowngrade(
            SQLiteDatabase db, int oldVersion, int newVersion
        ) {
        }
    }
}
