/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.media;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Bitmap;

import android.net.Uri;
import android.provider.MediaStore;
import android.webkit.MimeTypeMap;

import com.google.api.client.util.DateTime;

import java.io.File;
import java.sql.Timestamp;
import java.util.Arrays;

import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public class MediaObject {
    // MediaColumns.BUCKET_DISPLAY_NAME was first added in API level 29, which we
    // don't use. HOWEVER - it's still the same for images and videos.
    public static final String BUCKET_DISPLAY_NAME = "bucket_display_name";
    public static final String CREATED_COLUMN =
        MediaStore.MediaColumns.DATE_ADDED;
    public static final String ID_COLUMN = MediaStore.MediaColumns._ID;

    public static final String[] PROJECTION =
        new String[]{ID_COLUMN, MediaStore.MediaColumns.DATA, CREATED_COLUMN,
            BUCKET_DISPLAY_NAME};

    private int mId;
    private Timestamp mCreated;
    private String mPath;
    private String mBucket;
    private Uri mContentUri;

    private long mSize;
    private String mMimeType;
    private String mFilename;

    public MediaObject(Cursor cursor, Uri contentUri) {
        mId = cursor.getInt(columnIdx(ID_COLUMN));
        mPath = cursor.getString(columnIdx(MediaStore.MediaColumns.DATA));
        mBucket = cursor.getString(columnIdx(BUCKET_DISPLAY_NAME));
        mCreated = new Timestamp(cursor.getLong(columnIdx(CREATED_COLUMN)));

        File t = getFile();

        mSize = t.length();
        mFilename = t.getName();

        try {
            mMimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                mPath.substring(mPath.lastIndexOf('.') + 1));
        } catch (Exception e) {
            fatalLog(e);
            mMimeType = null;
        }

        if (mMimeType == null) {
            try {
                mMimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                    MimeTypeMap.getFileExtensionFromUrl(mPath));
            } catch (Exception e) {
                fatalLog(e);
                mMimeType = null;
            }
        }

        mContentUri = contentUri;
    }

    public static int columnIdx(String column_name) {
        return Arrays.asList(PROJECTION).indexOf(column_name);
    }

    public static String getSortValue() {
        return MediaStore.MediaColumns._ID + " DESC";
    }

    public Bitmap getThumbnail(ContentResolver resolver) {
        Bitmap thumbnail = null;
        if (!(getMimeType() == null)) {
            if (getMimeType().startsWith("image")) {
                thumbnail = MediaStore.Images.Thumbnails.getThumbnail(resolver,
                    mId, MediaStore.Images.Thumbnails.MINI_KIND, null
                );
            } else {
                thumbnail = MediaStore.Video.Thumbnails.getThumbnail(resolver,
                    mId, MediaStore.Video.Thumbnails.MINI_KIND, null
                );
            }
        }
        return thumbnail;
    }

    public int getId() {
        return mId;
    }

    public Timestamp getCreated() {
        return mCreated;
    }

    public String getPath() {
        return mPath;
    }

    public String getBucket() {
        return mBucket;
    }

    public long getBytes() {
        return mSize;
    }

    public String getMimeType() {
        return mMimeType;
    }

    public String getFilename() {
        return mFilename;
    }

    public File getFile() {
        return new File(mPath);
    }

    public DateTime getGoogleDateTime() {
        return new DateTime(getCreated().getTime() * 1000);
    }

    public void delete(ContentResolver resolver) {
        resolver.delete(mContentUri, ID_COLUMN + " = " + mId, null);
    }

    public void markSynced(SyncDatabaseLocal db, String g_account) {
        db.addEntry(getPath(), getId(), getCreated().getTime(), g_account);
    }
}
