/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.media.views;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tlucas.sheepsync.R;
import com.tlucas.sheepsync.asyncworkers.ThumbNailLoader;
import com.tlucas.sheepsync.media.MediaObject;
import com.tlucas.sheepsync.media.MediaViewArrayAdapter;

public class MediaViewHolder extends BaseViewHolder
    implements ThumbNailLoader.ThumbNailLoaded {
    private ContentResolver mResolver;
    private ThumbNailLoader mCurrentLoader = null;

    public MediaViewHolder(
        ViewGroup parent, MediaViewArrayAdapter parentAdapter, int layout_id
    ) {
        super(parent, parentAdapter, layout_id);
        mResolver = parent.getContext().getContentResolver();
    }

    public void setContent(int position) {
        MediaObject obj = (MediaObject)mAdapter.objects.objectAtPosition(position);
        ((ImageView) itemView.findViewById(R.id.IVThumb)).setImageBitmap(null);
        itemView.setTag(obj.getPath());
        if (mCurrentLoader != null) {
            mCurrentLoader.cancel(true);
        }
        mCurrentLoader = new ThumbNailLoader(mResolver, this);
        mCurrentLoader.execute(obj);
        itemView.findViewById(R.id.BTDontSync).setOnClickListener(
            (View v) -> mAdapter.remove(obj.getId()));
    }

    public void onThumbNailLoaded(Bitmap thumbnail) {
        ((ImageView) itemView.findViewById(R.id.IVThumb)).setImageBitmap(
            thumbnail);
        mCurrentLoader.cancel(true);
        mCurrentLoader = null;
    }
}
