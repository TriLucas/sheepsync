/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.media.views;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tlucas.sheepsync.R;
import com.tlucas.sheepsync.media.MediaViewArrayAdapter;

public class GalleryGroupHolder extends BaseViewHolder {
    public GalleryGroupHolder(
        ViewGroup parent, MediaViewArrayAdapter parentAdapter, int layout_id
    ) {
        super(parent, parentAdapter, layout_id);
    }

    public void setContent(int position) {
        TextView view = itemView.findViewById(R.id.TVImageGroup);
        String text = (String) mAdapter.objects.objectAtPosition(position);
        view.setText(text);
        view.setOnClickListener((View v) -> {
            mAdapter.hideGroup(text);
        });
        itemView.findViewById(R.id.BTDontSyncGroup).setOnClickListener(
            (View v) -> mAdapter.remove(text));
    }
}
