/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

import com.tlucas.sheepsync.R;

public class ReassureDialog extends DialogFragment {
    private DialogInterface.OnClickListener mListener;
    private int mMessage;

    public ReassureDialog(
        DialogInterface.OnClickListener listener, int message
    ) {
        mListener = listener;
        mMessage = message;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(mMessage).setPositiveButton(
            R.string.dialog_yes, mListener).setNegativeButton(
            R.string.dialog_no, (DialogInterface dialog, int which) -> {
            });

        return builder.create();
    }
}
