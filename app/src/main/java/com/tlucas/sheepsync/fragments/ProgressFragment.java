/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tlucas.sheepsync.asyncworkers.GoogleSyncForegroundService;
import com.tlucas.sheepsync.asyncworkers.GoogleSyncWorker;
import com.tlucas.sheepsync.dialogs.ReassureDialog;
import com.tlucas.sheepsync.R;

import static com.tlucas.sheepsync.shared.Logging.fatalLog;
import static com.tlucas.sheepsync.shared.SheepReporter.buildPrepMsg;
import static com.tlucas.sheepsync.shared.SheepReporter.buildProgress;
import static com.tlucas.sheepsync.shared.SheepReporter.buildReport;

public class ProgressFragment extends AutoCleaningFragment
    implements GoogleSyncWorker.ProgressUpdate,
    GoogleSyncForegroundService.GoogleFGSConnection.ConnectionListener {

    @DestroyOnly private ProgressBar mBar;
    @DestroyOnly private TextView mTWprogress;

    private View mProgressLayout;
    private View mReportLayout;

    private boolean mBound = false;
    private GoogleSyncForegroundService mService = null;
    private int mRemoteListenerId = -1;

    private int mTarget;

    private ServiceConnection mServiceConnection;

    public ProgressFragment() {
    }

    @Override
    public View onCreateView(
        LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        View layout = inflater.inflate(
            R.layout.fragment_progress, container, false);

        mProgressLayout = layout.findViewById(R.id.CLprogress);
        mReportLayout = layout.findViewById(R.id.CLreport);

        mBar = layout.findViewById(R.id.current_progress);
        mTWprogress = layout.findViewById(R.id.TWprogresss);
        layout.findViewById(R.id.BTcancel).setOnClickListener((View v) -> {
            ReassureDialog sure = new ReassureDialog(
                (DialogInterface dialog, int which) -> cancelSync(),
                R.string.sure_abort_dialog
            );
            sure.show(getActivity().getSupportFragmentManager(),
                getString(R.string.app_name)
            );
        });
        return layout;
    }

    private void connectToService() {
        mBound = false;
        mService = null;
        mRemoteListenerId = -1;

        mServiceConnection =
            new GoogleSyncForegroundService.GoogleFGSConnection(this);

        getActivity().bindService(
            new Intent(getActivity(), GoogleSyncForegroundService.class),
            mServiceConnection, Context.BIND_IMPORTANT
        );
    }

    private void disconnectFromService(boolean unregister) {
        try {
            if (mBound) {
                if (mService != null && mRemoteListenerId != -1 && unregister) {
                    mService.proxyRemoveListener(mRemoteListenerId);
                }
                getActivity().unbindService(mServiceConnection);
                mServiceConnection = null;
            }
        } catch (IllegalArgumentException e) {
            fatalLog(e);
        } finally {
            mServiceConnection = null;
            mService = null;
            mBound = false;
            mRemoteListenerId = -1;
        }
    }

    private void disconnectFromService() {
        disconnectFromService(true);
    }

    public void setService(GoogleSyncForegroundService service) {
        mService = service;
        if (!(mService instanceof GoogleSyncWorker.InterfaceProxy)) {
            throw new RuntimeException(
                mService.toString() + " must implement ProgressUpdate");
        }
        mRemoteListenerId = mService.proxyAddListener(ProgressFragment.this);
        mBound = true;
    }

    public void clearService() {
/* The service might still call methods of GoogleSyncWorker.ProgressUpdate -
   however, this fragment MUST NOT have direct access anymore. Clearing the
   reference to this fragement is done in the service / the Asynctask */
        mBound = false;
        mService = null;
        mRemoteListenerId = -1;
    }

    @Override
    public void onResume() {
        super.onResume();
        connectToService();
    }

    @Override
    public void onPause() {
        super.onPause();
        disconnectFromService();
    }

    public void cancelSync() {
        if (mService != null) {
            mService.cancel();
        }
    }

    private String formatTime(long time) {
        String[] units = new String[]{"h", "m", "s"};
        long[] vals = new long[]{time / 3600, (time % 3600) / 60, (time % 60)};
        StringBuilder res = new StringBuilder();

        for (int i = 0; i < 3; i++) {
            if (vals[i] > 0) {
                if (i > 0) {
                    res.append(" ");
                }
                res.append(vals[i] + units[i]);
            }
        }
        return res.toString();
    }

    public void onSyncerDone(
        int pushed, int skipped, long bytesPushed, long time
    ) {
        disconnectFromService(false);

        mProgressLayout.setVisibility(View.GONE);
        mReportLayout.setVisibility(View.VISIBLE);

        int cheers;
        if (mTarget - (pushed + skipped) == 0) {
            cheers = R.string.msg_cheer_done;
        } else {
            cheers = R.string.msg_cheer_almost_done;
        }
        ((TextView) mReportLayout.findViewById(R.id.TWCheerDone)).setText(
            cheers);
        ((TextView) mReportLayout.findViewById(R.id.TWDoneReport)).setText(
            buildReport(getContext(), pushed, skipped, mTarget, bytesPushed));

        ((TextView) mReportLayout.findViewById(R.id.TWTimeReport)).setText(
            getString(R.string.msg_report_time_taken, formatTime(time)));
    }

    public void setInitial(int payload, long bytesAll) {
        mTarget = payload;
        mBar.setMax(payload);
        mTWprogress.setText(buildPrepMsg(getContext(), payload));
        mBar.setProgress(0);
        mBar.setMin(0);
    }

    public void onProgressUpdate(
        int pushed, int skipped, int payload, long bytesPushed, long bytesAll
    ) {
        mTWprogress.setText(
            buildProgress(getContext(), pushed, skipped, payload, bytesPushed,
                bytesAll
            ));
        mBar.setProgress(pushed, true);
    }
}
