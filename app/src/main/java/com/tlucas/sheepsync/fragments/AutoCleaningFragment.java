/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.fragments;

import android.content.Context;

import androidx.fragment.app.Fragment;

import com.tlucas.sheepsync.fragments.interfaces.SwitchInterface;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public abstract class AutoCleaningFragment extends Fragment {
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @interface DestroyOnly {
    }

    private boolean isRetainable(Field field, boolean destroying) {
        if (!destroying) {
            Annotation[] annotations = field.getDeclaredAnnotations();
            for (Annotation a : annotations) {
                if (a instanceof DestroyOnly) {
                    return false;
                }
            }
        }

        return ((field.getModifiers() & Modifier.FINAL) == 0) &&
               !field.getType().isPrimitive();
    }

    private void nullField(Field field) {
        try {
            boolean accessible = field.isAccessible();
            field.setAccessible(true);
            field.set(this, null);
            field.setAccessible(accessible);
        } catch (IllegalAccessException e) {
        }
    }

    private Field[] findNullableFields(boolean destroying) {
        Field[] fields = this.getClass().getDeclaredFields();

        return Arrays.stream(fields).filter(f -> isRetainable(f, destroying))
            .toArray(Field[]::new);
    }

    @Override
    public void onStop() {
        for (Field f : findNullableFields(false)) {
            nullField(f);
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        for (Field f : findNullableFields(true)) {
            nullField(f);
        }
        super.onDestroy();
    }

    protected SwitchInterface getFragmentSwitcher() {
        Context context = getActivity();
        if (!(context instanceof SwitchInterface)) {
            throw new RuntimeException(
                context.toString() + " must implement SwitchInterface");
        } else {
            return (SwitchInterface) context;
        }
    }
}
