/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.FileProvider;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tlucas.sheepsync.asyncworkers.ApkDownloader;
import com.tlucas.sheepsync.asyncworkers.Downloader;
import com.tlucas.sheepsync.asyncworkers.JsonDownloader;
import com.tlucas.sheepsync.R;
import com.tlucas.sheepsync.shared.SharedPrefsManager;
import com.tlucas.sheepsync.updates.Version;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import static com.tlucas.sheepsync.BuildConfig.VERSION_NAME;
import static com.tlucas.sheepsync.shared.Logging.debugLog;
import static com.tlucas.sheepsync.shared.Logging.fatalLog;
import static com.tlucas.sheepsync.updates.BaseUpdates.APK_URL;
import static com.tlucas.sheepsync.updates.BaseUpdates.VERSION_URL;

public class UpdateFragment extends AutoCleaningFragment
    implements Downloader.DownloadListener {
    public static class DownloadedSettings extends SharedPrefsManager {
        private static final String PREFS_LAST_DOWNLOAD = "last_download";

        public static void putDownloadPath(Context context, String path) {
            getSharedPrefsEditor(context).putString(PREFS_LAST_DOWNLOAD, path)
                .apply();
        }

        public static String getDownloadPath(Context context) {
            return getSharedPrefs(context).getString(PREFS_LAST_DOWNLOAD, null);
        }

        public static void clearDownloadPath(Context context) {
            getSharedPrefsEditor(context).remove(PREFS_LAST_DOWNLOAD).apply();
        }
    }

    private Downloader mDownloader = null;
    private Version mOwnVersion;
    private String remoteFilename;

    private ProgressBar mPBprogress = null;
    private TextView mTWVersion = null;
    private Button mBTUpdate = null;

    public UpdateFragment() {
        mDownloader = new JsonDownloader(this);
        try {
            mDownloader.execute(new URL(VERSION_URL));
        } catch (Exception e) {
            fatalLog(e);
        }
    }

    @Override
    public View onCreateView(
        LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        View layout = inflater.inflate(
            R.layout.fragment_update, container, false);
        mPBprogress = layout.findViewById(R.id.PBcurrentProgress);
        ((TextView) layout.findViewById(R.id.TVLocalVersion)).setText(
            VERSION_NAME);
        mTWVersion = layout.findViewById(R.id.TVRemoteVersion);
        mBTUpdate = layout.findViewById(R.id.BTDoUpdate);

        mOwnVersion = new Version(VERSION_NAME);

        return layout;
    }

    @Override
    public void onStop() {
        if (mDownloader != null) {
            mDownloader.cancel(true);
        }
        super.onStop();
    }

    public void onDownload(String[] result) {
        mPBprogress.setMax(1);
        mPBprogress.setProgress(1);

        if (result == null || result.length == 0) {
            mTWVersion.setText(getString(R.string.update_remote_error));
        } else if (result.length == 2) {
            Version remoteVersion = new Version(result[0]);
            remoteFilename = result[1];

            mTWVersion.setText(remoteVersion.toString());
            int vgl = mOwnVersion.compareTo(remoteVersion);
            if (vgl < 0) {

                mBTUpdate.setEnabled(true);
                mBTUpdate.setText(getString(R.string.do_update));
                mBTUpdate.setOnClickListener((View v) -> {
                    doUpdate();
                    mBTUpdate.setEnabled(false);
                    mBTUpdate.setOnClickListener(null);
                });
            }
        } else if (result.length == 1) {
            debugLog(Log.WARN, "APK downloaded to " + result[0]);

            DownloadedSettings.putDownloadPath(getContext(), result[0]);

            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri uri = FileProvider.getUriForFile(getContext(),
                getContext().getPackageName() + ".provider", new File(result[0])
            );
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setDataAndType(
                uri, "application/vnd.android.package-archive");
            startActivity(intent);
        }
    }

    public void onUpdate(int progress) {
        mPBprogress.setProgress(progress);
    }

    public void setExpectedContentLength(int length) {
        mPBprogress.setMax(length);
    }

    public void doUpdate() {
        mDownloader = new ApkDownloader(
            this, Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS));

        try {
            mDownloader.execute(
                new URL(String.format(APK_URL, remoteFilename)));
        } catch (MalformedURLException e) {
            fatalLog(e);
            mDownloader = null;
        }
    }
}
