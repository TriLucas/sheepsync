/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tlucas.sheepsync.asyncworkers.GoogleSyncForegroundService;
import com.tlucas.sheepsync.asyncworkers.MediaScanner;
import com.tlucas.sheepsync.dialogs.ReassureDialog;
import com.tlucas.sheepsync.media.MediaObject;
import com.tlucas.sheepsync.media.MediaViewArrayAdapter;
import com.tlucas.sheepsync.perms.BasePermissions;
import com.tlucas.sheepsync.perms.GoogleLoginPermissions;
import com.tlucas.sheepsync.R;
import com.tlucas.sheepsync.shared.SharedPrefsManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ManualSyncFragment extends AutoCleaningFragment
    implements MediaScanner.ScannerCallback {
    public static class ManualSyncSettings extends SharedPrefsManager {
        private static final String PREFS_LAST_SCAN_AMOUNT = "last_scan_amount";
        private static final String PREFS_LAST_SCAN_COMPARATOR =
            "last_scan_comparator";
        private static final String PREFS_LAST_SCAN_UNIT = "scan_scan_unit";
        private static final String PREFS_LAST_ALL_UNSYNCED =
            "last_scan_unsynced";

        private static void putIntVal(Context ctx, String name, int val) {
            getSharedPrefsEditor(ctx).putInt(name, val).apply();
        }

        private static int getIntVal(Context ctx, String name, int defValue) {
            return getSharedPrefs(ctx).getInt(name, defValue);
        }

        public static void putLastAllUnsynced(Context ctx, boolean val) {
            getSharedPrefsEditor(ctx).putBoolean(PREFS_LAST_ALL_UNSYNCED, val)
                .apply();
        }

        public static boolean getLastAllUnsynced(
            Context ctx, boolean defValue
        ) {
            return getSharedPrefs(ctx).getBoolean(
                PREFS_LAST_ALL_UNSYNCED, defValue);
        }

        public static void putLastAmount(Context ctx, int val) {
            putIntVal(ctx, PREFS_LAST_SCAN_AMOUNT, val);
        }

        public static String getLastAmount(Context ctx) {
            return Integer.toString(getIntVal(ctx, PREFS_LAST_SCAN_AMOUNT, 2));
        }

        public static void putLastComparator(Context ctx, int val) {
            putIntVal(ctx, PREFS_LAST_SCAN_COMPARATOR, val);
        }

        public static int getLastComparator(Context ctx) {
            return getIntVal(ctx, PREFS_LAST_SCAN_COMPARATOR, 0);
        }

        public static void putLastTimeUnit(Context ctx, int val) {
            putIntVal(ctx, PREFS_LAST_SCAN_UNIT, val);
        }

        public static int getLastTimeUnit(Context ctx) {
            return getIntVal(ctx, PREFS_LAST_SCAN_UNIT, 0);
        }
    }

    private static final char[] COMPARAL_MAPPING = new char[]{'<', '>'};
    private static final int[] CALENDAR_MAPPING =
        new int[]{Calendar.MONTH, Calendar.WEEK_OF_YEAR, Calendar.DAY_OF_YEAR,
            Calendar.HOUR_OF_DAY};

    private MediaScanner mScannerTask = null;

    // Layout components needed by scan / execute
    private LinearLayout mFilterContainer;
    private RecyclerView mRecyclerView;
    private Button mBtScan;
    private Button mBtExecute;
    private TextView mNMedia;
    private EditText mETTimeSpan;
    private Spinner mSPSpan;
    private Spinner mSPvgl;
    private CheckBox mDoDelete;
    private CheckBox mAllUnsynced;

    public ManualSyncFragment() {
    }

    private void switchToProgress() {
        getFragmentSwitcher().switchFragment(new ProgressFragment());
    }

    private void toggleFilters(boolean val) {
        if (val) {
            mFilterContainer.setVisibility(View.GONE);
        } else {
            mFilterContainer.setVisibility(View.VISIBLE);
        }
    }

    private void doScan() {
        mBtExecute.setEnabled(false);
        mBtScan.setEnabled(false);

        boolean unsynced = mAllUnsynced.isChecked();
        int amount = Integer.parseInt(mETTimeSpan.getText().toString());
        int comparator = mSPvgl.getSelectedItemPosition();
        int unit = mSPSpan.getSelectedItemPosition();

        char cmp = COMPARAL_MAPPING[comparator];

        Calendar cal = Calendar.getInstance();
        cal.add(CALENDAR_MAPPING[unit], -amount);

        if (mScannerTask != null) {
            mScannerTask.cancel(true);
        }

        Context context = getContext();

        ManualSyncSettings.putLastAmount(context, amount);
        ManualSyncSettings.putLastComparator(context, comparator);
        ManualSyncSettings.putLastTimeUnit(context, unit);
        ManualSyncSettings.putLastAllUnsynced(context, unsynced);

        mScannerTask = new MediaScanner(requireContext(), this,
            cal.getTimeInMillis() / 1000, cmp, unsynced
        );
        mScannerTask.execute();

    }

    public void onScanDone(MediaViewArrayAdapter media) {
        int items = media.objects.getActualCount();

        media.registerAdapterDataObserver(
            new RecyclerView.AdapterDataObserver() {
                @Override
                public void onItemRangeRemoved(
                    int positionStart, int itemCount
                ) {
                    int items = ((MediaViewArrayAdapter) mRecyclerView
                        .getAdapter()).objects.getActualCount();
                    mNMedia.setText(getString(R.string.msg_n_found_media,
                        getResources()
                            .getQuantityString(R.plurals.base_n_files, items,
                                items
                            )
                    ));
                }
            });

        mBtExecute.setEnabled(items > 0);
        mRecyclerView.setAdapter(media);
        mNMedia.setText(getString(R.string.msg_n_found_media, getResources()
            .getQuantityString(R.plurals.base_n_files, media.getItemCount(),
                items
            )));
        mBtScan.setEnabled(true);
        mScannerTask.cancel(true);
        mScannerTask = null;
    }

    private void doSync() {
        MediaViewArrayAdapter adapter;
        if ((adapter = (MediaViewArrayAdapter) mRecyclerView.getAdapter()) !=
            null) {
            List<MediaObject> raw = adapter.objects.getRawData();
            ArrayList<Integer> ids = new ArrayList<>();

            for (int i = 0; i < raw.size(); i++) {
                ids.add(raw.get(i).getId());
            }

            Intent serviceIntent = new Intent(
                requireContext(), GoogleSyncForegroundService.class);
            serviceIntent.putIntegerArrayListExtra("ids", ids);
            serviceIntent.putExtra("delete", mDoDelete.isChecked());

            getActivity().startForegroundService(serviceIntent);
            switchToProgress();
        }
    }

    @Override
    public View onCreateView(
        LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        View layout = inflater.inflate(
            R.layout.fragment_manual_sync, container, false);

        mNMedia = layout.findViewById(R.id.TVfoundImages);
        mETTimeSpan = layout.findViewById(R.id.EDTimeSpan);
        mSPvgl = layout.findViewById(R.id.SPvgl);
        mSPSpan = layout.findViewById(R.id.SPspan);
        mDoDelete = layout.findViewById(R.id.CBdelete);
        mFilterContainer = layout.findViewById(R.id.LLspinners);

        mBtExecute = layout.findViewById(R.id.BTexecute);
        mBtExecute.setOnClickListener((View v) -> {
            GoogleLoginPermissions perms = new GoogleLoginPermissions(this);
            if (perms.isGranted()) {
                ReassureDialog sure = new ReassureDialog(
                    (DialogInterface dialog, int which) -> doSync(),
                    R.string.you_sure_prompt_start_sync
                );
                sure.show(getActivity().getSupportFragmentManager(),
                    getString(R.string.app_name)
                );
            } else {
                getFragmentSwitcher().switchFragment(
                    new MainFragment(perms.getCode()));
            }
        });

        mBtScan = layout.findViewById(R.id.BTscan);
        mBtScan.setOnClickListener((View v) -> {
            BasePermissions perms = new BasePermissions(this);
            if (perms.isGranted()) {
                doScan();
            } else {
                getFragmentSwitcher().switchFragment(
                    new MainFragment(perms.getCode()));
            }
        });

        mAllUnsynced = layout.findViewById(R.id.CBunsynced);
        mAllUnsynced.setOnClickListener((View v) -> {
            toggleFilters(((CheckBox) v).isChecked());
        });

        Context context = getContext();

        mETTimeSpan.setText(ManualSyncSettings.getLastAmount(context));
        mSPSpan.setSelection(ManualSyncSettings.getLastTimeUnit(context));
        mSPvgl.setSelection(ManualSyncSettings.getLastComparator(context));
        mAllUnsynced.setChecked(
            ManualSyncSettings.getLastAllUnsynced(context, false));

        toggleFilters(mAllUnsynced.isChecked());

        mRecyclerView = layout.findViewById(R.id.GVimages);
        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (GoogleSyncForegroundService.isRunning()) {
            switchToProgress();
        }
    }

    @Override
    public void onStop() {
        mBtExecute.setOnClickListener(null);
        mBtScan.setOnClickListener(null);
        mAllUnsynced.setOnClickListener(null);
        super.onStop();
    }
}
