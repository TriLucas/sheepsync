/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.fragments.interfaces;

import androidx.fragment.app.Fragment;

public interface SwitchInterface {
    void switchFragment(Fragment next);
    void switchFragment(Fragment next, boolean toBackStack);
    void popBackStack();
}
