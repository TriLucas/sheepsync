/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.work.WorkInfo;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.tlucas.sheepsync.bcreceivers.MediaWorkerBroadcastReceiver;
import com.tlucas.sheepsync.perms.base.PermissionSetObject;
import com.tlucas.sheepsync.perms.BasePermissions;
import com.tlucas.sheepsync.perms.GoogleLoginPermissions;
import com.tlucas.sheepsync.R;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;
import java.util.List;

import static com.tlucas.sheepsync.MainActivity.MainSettings.getInstalledTstamp;
import static com.tlucas.sheepsync.asyncworkers.MediaWatcherWorker.cancelWorkers;
import static com.tlucas.sheepsync.asyncworkers.MediaWatcherWorker.enqueueUnique;
import static com.tlucas.sheepsync.asyncworkers.MediaWatcherWorker.getIsRunning;
import static com.tlucas.sheepsync.MainActivity.MainSettings.getAutoAsk;
import static com.tlucas.sheepsync.MainActivity.MainSettings.putAutoAsk;
import static com.tlucas.sheepsync.MainActivity.MainSettings.putAutoBoot;
import static com.tlucas.sheepsync.media.MediaQuery.QuerySettings.getNewestTimestamp;
import static com.tlucas.sheepsync.shared.Logging.fatalLog;

public class MainFragment extends AutoCleaningFragment implements
    MediaWorkerBroadcastReceiver.MediaWorkUpdateInterface {
    @DestroyOnly
    private SparseArray<PermissionSetObject> mPermissions = null;
    @DestroyOnly
    private Button mToggler;
    @DestroyOnly
    private Button mGrant;
    @DestroyOnly
    private TextView mTVPermStatus;
    @DestroyOnly
    private TextView mTVStatus;

    private MediaWorkerBroadcastReceiver mReceiver;

    private int mNeededPerm = -1;

    public MainFragment() {
    }

    public MainFragment(int requestCode) {
        mNeededPerm = requestCode;
    }

    private void addPermissionSet(PermissionSetObject obj) {
        if (mPermissions == null) {
            mPermissions = new SparseArray<>();
        }
        mPermissions.put(obj.getCode(), obj);
    }

    private void buildPermissions() {
        addPermissionSet(new BasePermissions(this));
        addPermissionSet(new GoogleLoginPermissions(this));
    }

    private int nextMissingPermission() {
        int result = -1;
        for (int i = 0; i < mPermissions.size(); i++) {
            PermissionSetObject o = mPermissions.valueAt(i);
            if (!(o.isGranted())) {
                result = i;
                break;
            }
        }
        return result;
    }

    private void askNextPerm() {
        int idx;
        if ((idx = nextMissingPermission()) != -1) {
            mPermissions.valueAt(idx).requestGrant();
        }
    }

    public void onRequestPermissionsResult(
        int requestCode, String[] permissions, int[] grantResults
    ) {
        mPermissions.get(requestCode).permissionsCallback(
            permissions, grantResults);
        displayPermsStatus();
        if (requestCode == mNeededPerm) {
            getFragmentSwitcher().popBackStack();
        } else {
            askNextPerm();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mPermissions.get(requestCode).activityCallback(resultCode, data);
        displayPermsStatus();
        if (requestCode == mNeededPerm) {
            getFragmentSwitcher().popBackStack();
        } else {
            askNextPerm();
        }
    }

    private void displayLastTimeFormatted(TextView target) {
        long tStamp;
        if ((tStamp = getNewestTimestamp(getContext()) * 1000) !=
            getInstalledTstamp(getContext()) * 1000) {
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(tStamp);

            DateFormat dateFormatter = DateFormat.getDateTimeInstance(
                DateFormat.MEDIUM, DateFormat.MEDIUM);
            dateFormatter.setTimeZone(cal.getTimeZone());

            target.setText(dateFormatter.format(cal.getTime()));
        } else {
            target.setText(R.string.msg_ran_not_yet);
        }
    }

    private void displayTextColored(TextView target, int msg, int color) {
        target.setText(getString(msg));
        target.setTextColor(getContext().getColor(color));
    }

    private void displayAutomaticWorkerStatus() {
        try {
            boolean isAutomatic = false;
            List<WorkInfo> worker = getIsRunning(getContext());
            for (WorkInfo bar : worker) {
                if (bar.getState().equals(WorkInfo.State.ENQUEUED)) {
                    isAutomatic = true;
                    break;
                }
            }
            if (isAutomatic) {
                displayTextColored(
                    mTVStatus, R.string.msg_is_active, R.color.colorActive);
                mToggler.setText(getString(R.string.bt_not_automatic_label));
                mToggler.setEnabled(true);
                mToggler.setOnClickListener((View v) -> {
                    putAutoBoot(getContext(), false);
                    cancelWorkers(getContext());
                    displayAutomaticWorkerStatus();
                });
            } else {
                displayTextColored(mTVStatus, R.string.msg_is_not_active,
                    R.color.colorInactive
                );
                mToggler.setText(getString(R.string.bt_do_automatic_label));
                mToggler.setEnabled(true);
                mToggler.setOnClickListener((View v) -> {
                    putAutoBoot(getContext(), true);
                    enqueueUnique(getContext());
                    displayAutomaticWorkerStatus();
                });
            }
        } catch (ExecutionException | InterruptedException e) {
            displayTextColored(mTVStatus, R.string.msg_active_check_error,
                R.color.colorInactive
            );
            mToggler.setText(getString(R.string.msg_active_check_error));
            mToggler.setEnabled(false);
        }
    }

    private void displayPermsStatus() {
        int idx;
        if ((idx = nextMissingPermission()) == -1) {
            displayTextColored(
                mTVPermStatus, R.string.msg_perms_granted, R.color.colorActive);
            mGrant.setVisibility(View.GONE);
            mGrant.setOnClickListener(null);
        } else {
            displayTextColored(mTVPermStatus, R.string.msg_perms_not_granted,
                R.color.colorInactive
            );
            mGrant.setVisibility(View.VISIBLE);
            mGrant.setOnClickListener(
                (View v) -> mPermissions.valueAt(idx).requestGrant());
        }
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
    }

    @Override
    public View onCreateView(
        LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState
    ) {
        buildPermissions();
        View layout = inflater.inflate(
            R.layout.fragment_main, container, false);
        layout.findViewById(R.id.BTmanual).setOnClickListener((View v) -> {
            Fragment manualFragment = new ManualSyncFragment();
            getFragmentSwitcher().switchFragment(manualFragment);
        });
        TextView tvlast = layout.findViewById(R.id.TVLast);

        mTVStatus = layout.findViewById(R.id.TVStatus);
        mToggler = layout.findViewById(R.id.BTToggleAutomation);
        mGrant = layout.findViewById(R.id.BTGrantPerms);
        mTVPermStatus = layout.findViewById(R.id.TWPerms);

        displayLastTimeFormatted(tvlast);
        displayAutomaticWorkerStatus();
        displayPermsStatus();

        return layout;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (getAutoAsk(getContext())) {
            putAutoAsk(getContext(), false);

            if (nextMissingPermission() != -1) {
                mPermissions.valueAt(0).requestGrant();
            }
        }

        if (mNeededPerm != -1) {
            mPermissions.valueAt(mPermissions.indexOfKey(mNeededPerm))
                .requestGrant();
        }
    }

    @Override
    public void onDestroy() {
        if (mGrant != null) {
            mGrant.setOnClickListener(null);
        }
        if (mToggler != null) {
            mToggler.setOnClickListener(null);
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        mReceiver = new MediaWorkerBroadcastReceiver(getContext(), this);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mReceiver.unregister(getContext());
        mReceiver = null;
    }

    public void onStartWorker() {

    }

    public void onStopWorker() {
        try {
            TextView t = getActivity().findViewById(R.id.TVLast);
            displayLastTimeFormatted(t);
        } catch (Throwable t) {
            fatalLog(t);
        }
    }
}
