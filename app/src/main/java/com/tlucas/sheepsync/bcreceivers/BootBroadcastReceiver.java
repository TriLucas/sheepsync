/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.bcreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tlucas.sheepsync.asyncworkers.MediaWatcherWorker;

import static com.tlucas.sheepsync.MainActivity.MainSettings.getAutoBoot;

public class BootBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals(Intent.ACTION_BOOT_COMPLETED) &&
            getAutoBoot(context)) {
            MediaWatcherWorker.enqueueUnique(context);
        }
    }
}
