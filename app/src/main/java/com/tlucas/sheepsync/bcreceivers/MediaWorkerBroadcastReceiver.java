/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.bcreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import static com.tlucas.sheepsync.asyncworkers.MediaWatcherWorker.WORKER_DONE_INTENT;
import static com.tlucas.sheepsync.asyncworkers.MediaWatcherWorker.WORKER_STARTED_INTENT;

public class MediaWorkerBroadcastReceiver extends BroadcastReceiver {
    private MediaWorkUpdateInterface mInterface;

    public MediaWorkerBroadcastReceiver(
        Context context, MediaWorkUpdateInterface receiver
    ) {
        mInterface = receiver;

        IntentFilter filter = new IntentFilter();
        filter.addAction(WORKER_STARTED_INTENT);
        filter.addAction(WORKER_DONE_INTENT);

        LocalBroadcastManager.getInstance(context).registerReceiver(
            this, filter);
    }

    public MediaWorkerBroadcastReceiver(Context context) {
        this(context, (MediaWorkUpdateInterface) context);
        if (!(context instanceof MediaWorkUpdateInterface)) {
            throw new RuntimeException(context.toString() +
                                       " must implement MediaWorkUpdateInterface");
        }
    }

    public void unregister(Context context) {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(this);
        mInterface = null;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (WORKER_DONE_INTENT.equals(intent.getAction())) {
            mInterface.onStopWorker();
        } else if (WORKER_STARTED_INTENT.equals(intent.getAction())) {
            mInterface.onStartWorker();
        }
    }

    public interface MediaWorkUpdateInterface {
        void onStartWorker();

        void onStopWorker();
    }
}
