/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.GrantPermissionRule;

import com.tlucas.sheepsync.perms.BasePermissions;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastAllUnsynced;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastComparator;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LifeCycleTests {
    private Context mContext;
    @Rule
    public TestsBasics.SharedPrefsActivityTestRule<MainActivity> activityRule =
        new TestsBasics.SharedPrefsActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(
        BasePermissions.EXPLICIT_PERMISSIONS);

    @Before
    public void init() {
        mContext =
            InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @Test
    public void clickThroughTheApp() {
        openActionBarOverflowOrOptionsMenu(mContext);
        onView(withText(R.string.action_about)).perform(click());

        onView(withId(R.id.IVtheSheep)).perform(pressBack());
        putLastComparator(mContext, 1);
        putLastAllUnsynced(mContext, false);
        onView(withId(R.id.BTmanual)).perform(click());

        onView(withId(R.id.BTscan)).perform(click());

        openActionBarOverflowOrOptionsMenu(mContext);
        onView(withText(R.string.action_updates)).perform(click());

        onView(withId(R.id.TVRemoteVersion)).perform(pressBack());
    }
}
