/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import static com.tlucas.sheepsync.MainActivity.MainSettings.putAutoAsk;
import static com.tlucas.sheepsync.MainActivity.MainSettings.putAutoBoot;
import static com.tlucas.sheepsync.MainActivity.MainSettings.putInstalledTstamp;
import static com.tlucas.sheepsync.MainActivity.newTimestamp;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastAmount;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastComparator;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastTimeUnit;
import static com.tlucas.sheepsync.media.MediaQuery.QuerySettings.putNewestTimestamp;

public class TestsBasics {
    public static class SharedPrefsActivityTestRule<T> extends ActivityTestRule {
        SharedPrefsActivityTestRule(Class<T> activityClass) {
            super(activityClass, false);
            setupData();
        }

        protected void setupData() {
            Context context =
                InstrumentationRegistry.getInstrumentation().getTargetContext();
            putAutoAsk(context, false);
            putAutoBoot(context, false);
            putInstalledTstamp(context, newTimestamp());
            putNewestTimestamp(context, newTimestamp());
            putLastAmount(context, 2);
            putLastComparator(context, 0);
            putLastTimeUnit(context, 0);
        }
    }
}
