/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.tlucas.sheepsync.MainActivity.MainSettings.getAutoBoot;
import static com.tlucas.sheepsync.TestsBasics.SharedPrefsActivityTestRule;
import static org.junit.Assert.assertEquals;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class WorkerServiceTests {
    private Context mContext;
    @Rule
    public SharedPrefsActivityTestRule<MainActivity> activityRule =
        new SharedPrefsActivityTestRule<>(MainActivity.class);

    @Before
    public void init() {
        mContext =
            InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    private class PairedToggledViewAction {
        private int mBtnIdCur;
        private int mBtnIdNext;

        private int mLabelIdCur;
        private int mLabelIdNext;

        private boolean nextAutoBoot;

        public PairedToggledViewAction(boolean autoboot) {
            mBtnIdCur = R.string.bt_not_automatic_label;
            mBtnIdNext = R.string.bt_do_automatic_label;
            mLabelIdCur = R.string.msg_is_active;
            mLabelIdNext = R.string.msg_is_not_active;

            nextAutoBoot = !autoboot;

            if (autoboot) {
                swap();
            } else {
                System.out.println("NEXT LABEL SHOULD BE: " +
                                   activityRule.getActivity().getResources()
                                       .getString(mLabelIdCur));
                System.out.println("NEXT BTN SHOULD BE: " +
                                   activityRule.getActivity().getResources()
                                       .getString(mBtnIdCur));
            }
        }

        private void swap() {
            int t;

            t = mBtnIdNext;
            mBtnIdNext = mBtnIdCur;
            mBtnIdCur = t;

            t = mLabelIdNext;
            mLabelIdNext = mLabelIdCur;
            mLabelIdCur = t;

            nextAutoBoot = !nextAutoBoot;

            System.out.println("NEXT LABEL SHOULD BE: " +
                               activityRule.getActivity().getResources()
                                   .getString(mLabelIdCur));
            System.out.println("NEXT BTN SHOULD BE: " +
                               activityRule.getActivity().getResources()
                                   .getString(mBtnIdCur));
        }

        public PairedToggledViewAction next() {
            onView(withId(R.id.BTToggleAutomation)).perform(click()).check(
                matches(withText(mBtnIdCur)));
            onView(withText(mLabelIdCur)).check(matches(withId(R.id.TVStatus)));

            assertEquals(nextAutoBoot, getAutoBoot(mContext));

            swap();
            return this;
        }
    }

    @Test
    public void workerQueueingTimes20() {
        boolean autoBoot = getAutoBoot(mContext);
        PairedToggledViewAction toggler = new PairedToggledViewAction(autoBoot);

        for (int i = 0; i < 20; i++) {
            toggler.next();
        }
    }
}
