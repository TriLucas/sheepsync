/* Copyright 2019 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.runner.screenshot.ScreenCapture;
import androidx.test.runner.screenshot.Screenshot;

import com.tlucas.sheepsync.media.MediaObject;
import com.tlucas.sheepsync.media.MediaQuery;
import com.tlucas.sheepsync.media.MediaViewArrayAdapter;
import com.tlucas.sheepsync.perms.BasePermissions;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.contrib.RecyclerViewActions.scrollToHolder;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withTagValue;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.getLastAmount;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.getLastComparator;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.getLastTimeUnit;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastAllUnsynced;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastAmount;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastComparator;
import static com.tlucas.sheepsync.fragments.ManualSyncFragment.ManualSyncSettings.putLastTimeUnit;
import static com.tlucas.sheepsync.helpers.ViewHolderMatchers.withImageView;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.Matchers.allOf;
import static com.tlucas.sheepsync.TestsBasics.SharedPrefsActivityTestRule;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class UiTests {
    private List<MediaObject> mCreatedScreenshots = new ArrayList<>();
    private Context mContext;
    @Rule
    public SharedPrefsActivityTestRule<MainActivity> activityRule =
        new SharedPrefsActivityTestRule<>(MainActivity.class);

    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(
        BasePermissions.EXPLICIT_PERMISSIONS);


    private void captureScreenshot(String name, Context context) {
        ScreenCapture capture = Screenshot.capture();
        capture.setFormat(Bitmap.CompressFormat.PNG);
        capture.setName(name);

        Bitmap data = capture.getBitmap();

        String newUri = MediaStore.Images.Media.insertImage(
                context.getContentResolver(), data, name, name);

        Long newId = ContentUris.parseId(Uri.parse(newUri));
        MediaQuery query = new MediaQuery(new Integer[]{newId.intValue()}, mContext.getContentResolver());
        mCreatedScreenshots.addAll(query.execute());
    }

    private ViewInteraction buttonDontSyncForTestScreenShot(int index) {
        Matcher<View> tagMatcher = withTagValue(is(mCreatedScreenshots.get(index).getPath()));
        onView(withId(R.id.GVimages)).perform(scrollToHolder(withImageView(tagMatcher)));
        return onView(allOf(withId(R.id.BTDontSync), withParent(tagMatcher)));
    }

    private int getCurrentRecyclerViewItemCount() {
        RecyclerView recyclerView = activityRule.getActivity().findViewById(R.id.GVimages);
        MediaViewArrayAdapter adapter = (MediaViewArrayAdapter)recyclerView.getAdapter();
        return adapter.objects.getActualCount();
    }

    private void clickToScan(int amount, int unit_id, boolean unsycnced) {
        // Go to the the "ManualSync" fragment and adjust settings
        onView(withText(R.string.bt_do_manual)).perform(click());

        if (!unsycnced) {

            // Set comparator
            onView(withId(R.id.SPvgl)).perform(click());
            onView(withText(R.string.newer_than)).perform(click());

            if (amount != -1) {
                // Enter amount
                onView(withId(R.id.EDTimeSpan)).perform(clearText()).perform(
                    typeText(Integer.toString(amount)));
            }

            if (unit_id != -1) {
                // Set unit
                onView(withId(R.id.SPspan)).perform(click());
                onView(withText(unit_id)).perform(click());
            }
        } else {
            onView(withId(R.id.CBunsynced)).perform(click());
        }

        // Perform the scan
        onView(withText(R.string.scan)).perform(click());
    }

    private void clickToScan() {
        clickToScan(-1, -1, false);
    }

    private void clickToScanUnsynced() { clickToScan(-1, -1, true); }

    @Before
    public void init() {
        mContext =
            InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @Test
    public void performScanAndBTDontSync() {
        // Perform a screenshot which we can show
        captureScreenshot("HomeScreen", mContext);

        // Do another screenshot, which will be removed from the list
        captureScreenshot("HomeScreen", mContext);

        clickToScan();

        // Check if Screenshot 1 is listed
        buttonDontSyncForTestScreenShot(0).check(matches(isDisplayed()));

        int before = getCurrentRecyclerViewItemCount();
        System.out.println("CURRENT COUNT: " + before);

        // Check if Screenshot 2 is listed and removed after Button click
        buttonDontSyncForTestScreenShot(1).check(matches(isDisplayed())).perform(click());
        int after = getCurrentRecyclerViewItemCount();
        assertEquals(before, after + 1);

        // Check if displayed Text matches the new amount
        onView(withId(R.id.TVfoundImages)).check(matches(withText(mContext
            .getString(R.string.msg_n_found_media,
                activityRule.getActivity().getResources()
                    .getQuantityString(R.plurals.base_n_files, after, after)
            ))));

    }

    public void keepTrackIntegrity() {
        putLastAllUnsynced(mContext, false);

        clickToScanUnsynced();

        // Get the amount of last untracked media
        onView(withId(R.id.BTscan)).perform(click());

        int before = getCurrentRecyclerViewItemCount();

        // Do another screenshot, which will should be added to the "unsynced" list
        captureScreenshot("HomeScreen", mContext);


        // Get the amount of last untracked media
        onView(withId(R.id.BTscan)).perform(click());

        int after = getCurrentRecyclerViewItemCount();
        assertEquals(before + 1, after);
    }

    private void preseedSettings(int amount) {
        putLastAmount(mContext, amount);
        putLastComparator(mContext, 0);
        putLastTimeUnit(mContext, 2);
        putLastAllUnsynced(mContext, false);
    }

    @Test
    public void assertSettingsSaved() {
        preseedSettings(2);
        int amount = Math.abs(new Random().nextInt(2000)) + 1;
        clickToScan(amount, R.string.span_weeks, false);

        assertEquals(amount, Integer.parseInt(getLastAmount(mContext)));
        assertEquals(1, getLastComparator(mContext));
        assertEquals(1, getLastTimeUnit(mContext));
    }

    @Test
    public void assertSettingsRead() {
        int amount = Math.abs(new Random().nextInt(2000)) + 1;
        preseedSettings(amount);

        // Go to the the "ManualSync" fragment
        onView(withText(R.string.bt_do_manual)).perform(click());

        onView(withId(R.id.EDTimeSpan)).check(
            matches(withText(Integer.toString(amount))));

        onView(withText(R.string.span_days)).check(matches(isDisplayed()));
        onView(withText(R.string.span_months)).check(doesNotExist());
    }

    @Test
    public void assertAllUnsyncedSettingRead() {
        putLastAllUnsynced(mContext, true);

        // Go to the the "ManualSync" fragment
        onView(withText(R.string.bt_do_manual)).perform(click());

        onView(withId(R.id.EDTimeSpan)).check(
            matches(withEffectiveVisibility(ViewMatchers.Visibility.GONE)));
    }

    @After
    public void cleanUp() {
        ContentResolver resolver = mContext.getContentResolver();
        for (MediaObject obj : mCreatedScreenshots) {
            obj.delete(mContext.getContentResolver());
        }
    }
}
