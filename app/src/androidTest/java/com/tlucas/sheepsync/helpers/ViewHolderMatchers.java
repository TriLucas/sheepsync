/* Copyright 2020 Tristan Lucas
 * SPDX-License-Identifier: Apache-2.0 */
package com.tlucas.sheepsync.helpers;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.espresso.matcher.BoundedMatcher;

import com.tlucas.sheepsync.media.views.MediaViewHolder;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class ViewHolderMatchers {
    public static Matcher<RecyclerView.ViewHolder> withImageView(final Matcher<View> tag) {
        return new BoundedMatcher<RecyclerView.ViewHolder, MediaViewHolder>(MediaViewHolder.class) {
            private boolean found = false;

            @Override
            public void describeTo(Description description) {
                description.appendText("with ImageView tagged with :" + tag);
            }

            @Override
            protected boolean matchesSafely(MediaViewHolder item) {
                found = tag.matches(item.itemView);
                return found;
            }
        };
    }
}
