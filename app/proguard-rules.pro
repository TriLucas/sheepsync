# Copyright 2019 Tristan Lucas
# SPDX-License-Identifier: Apache-2.0
-keep class org.xmlpull.** { *; }

-keep class com.tlucas.** { *; }
-keep class androidx.lifecycle.service.** { *; }
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}
# Fix OAuth Drive API failure for release builds
-keep class * extends com.google.api.client.json.GenericJson { *; }
-keep class com.google.api.services.drive.** { *; }
-keepclassmembers class * { @com.google.api.client.util.Key <fields>; }

-dontobfuscate
-printusage build/removed.txt
