# SheepSync for Android

## Trivia

SheepSync will automatically take care of syncing new media (images,
videos) you receive to your Google Drive.

## Requirements

 * Android Oreo (8.0.0) or higher
 * A [Google][google] account

## Installation

On your Smartphone, download the latest version of SheepSync from
[here][download].

*You may need to permit your Browser to install APKs.*

## Known Limitations:

* On Huawei devices, the **[Huawei Protected Apps][huawei_system_manager]
$!*"%!** prevents the Service
from starting up automatically on Boot. Please follow
[these instructions][huawei_instructions], if you want the automatic
booting to work properly.

## Disclaimer

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

# Contributing
## Linting

Run the following command to lint your changes for your current variant:

    $ ./gradlew lint

[google]: www.google.com
[huawei_system_manager]: https://stackoverflow.com/questions/31638986/protected-apps-setting-on-huawei-phones-and-how-to-handle-it
[huawei_instructions]: https://www.digitalcitizen.life/stop-huawei-from-closing-apps-when-you-lock-screen
[download]: https://trilucas.gitlab.io/sheepsync/