# Copyright 2019 Tristan Lucas
# SPDX-License-Identifier: Apache-2.0
import argparse
import io
import markdown
import os
import requests
from dateutil import parser as dparser
from gitlab import Gitlab
from jinja2 import Environment, FileSystemLoader
from json import JSONDecodeError
from markupsafe import Markup
from markdown.preprocessors import Preprocessor

ENV = Environment(loader=FileSystemLoader(os.path.dirname(__file__)))


class StripSections(Preprocessor):
    def __init__(self, sections, *args, **kwargs):
        self._sections = sections
        super(StripSections, self).__init__(*args, **kwargs)

    def run(self, lines):
        skipping = False
        result = []
        for line in lines:
            if line.startswith('# '):
                skipping = True
            if line.startswith('##'):
                skipping = False
                for section in self._sections:
                    if section in line:
                        skipping = True

            if not skipping:
                result.append(line)
        return result


class StripSectionsExt(markdown.extensions.Extension):
    def __init__(self, sections, *args, **kwargs):
        self.sections = sections
        super(StripSectionsExt, self).__init__(*args, **kwargs)

    def extendMarkdown(self, md, md_globals):
        md.registerExtension(self)
        md.preprocessors.add(
            'strip_sections',
            StripSections(self.sections),
            '>normalize_whitespace'
        )


class ReleaseInfo(object):
    def __init__(self, apk, apk_link, history_link, finished, commit,
                 is_current=False):
        self.apk = apk
        self.apk_link = apk_link
        self.history_link = history_link
        self.commit = commit
        self.finished = dparser.parse(finished).strftime('%d.%m.%Y - %H:%M')
        self.is_current = is_current

    def __str__(self):
        tmpl = 'current_release.html' if self.is_current else 'release.html'
        return Markup(
            ENV.get_template(tmpl).render(**(self.__dict__))
        )


class JobGitlabConnection(Gitlab):
    output_filename = 'artifacts/file/app/build/outputs/apk/{}/{}'

    @classmethod
    def get_configured(cls):
        access_token = os.environ.get('PAT')
        gitlab_root_url = 'https://' + os.environ.get('CI_SERVER_HOST')
        gitlab_ssl_verify = True
        gitlab_api_version = 4

        return cls(
            gitlab_root_url,
            private_token=access_token,
            ssl_verify=gitlab_ssl_verify,
            api_version=gitlab_api_version,
        )

    def get_release_info_from_job(self, job, channel):
        json_url = os.path.join(
            job.web_url,
            self.output_filename.format(channel, 'output.json')
        )

        apk = None
        try:
            apk = requests.get(json_url).json()[0]['apkData']['outputFile']
        except JSONDecodeError:
            apk = 'Removed Version'

        project_url = os.environ.get('CI_PROJECT_URL')
        apk_link = os.path.join(
            job.web_url, self.output_filename.format(channel, apk)
        )

        return ReleaseInfo(
            apk,
            apk_link,
            '{}/commits/{}/'.format(project_url, job.pipeline['sha']),
            job.finished_at,
            job.pipeline['sha'],
            str(job.pipeline['id']) == str(os.environ.get('CI_PIPELINE_ID')),
        )

    def get_revision_pipelines(self):
        project = self.projects.get(id=os.environ.get('CI_PROJECT_ID'),
                                    lazy=True)

        yield project.pipelines.get(id=os.environ.get('CI_PIPELINE_ID'))

        pipelines = project.pipelines.list(
            scope='finished', status='success',
            ref=os.environ.get('CI_COMMIT_REF_NAME'), order_by='id',
            sort='desc', per_page=os.environ.get('MAX_RELEASES', 20)
        )

        for pipeline in pipelines:
            yield pipeline

    def get_all_relase_artifacts(self, release_job_name, channel):
        for pipeline in self.get_revision_pipelines():
            for job in pipeline.jobs.list(scope='success', order_by='id',
                                          sort='desc'):
                if job.name == release_job_name:
                    yield self.get_release_info_from_job(job, channel)


def render_readme():
    result = None

    with io.open('README.md', 'r') as fp:
        result = Markup(
            markdown.markdown(
                fp.read(),
                extensions=[StripSectionsExt(('Installation', 'Linting'))]
            )
        )
    return result

def render_index(args):
    gitlab = JobGitlabConnection.get_configured()
    previous = gitlab.get_all_relase_artifacts(args.release_job, args.channel)

    template = ENV.get_template('index.html')

    target_file = os.path.join(os.getcwd(), args.target, 'index.html')

    with io.open(target_file, 'w') as fp:
        fp.write(template.render(
            revision=os.environ.get('CI_COMMIT_SHORT_SHA'),
            releases=previous,
            readme=render_readme(),
        ))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""Build a new GitLab page.
    THIS MUST NOT BE RUN LOCALLY!""")
    parser.add_argument('-t', '--target',
                        type=str, default='public')
    parser.add_argument('-j', '--release-job',
                        type=str, default='build:app:release')
    parser.add_argument('-c', '--channel',
                        type=str, default='release')

    args = parser.parse_args()

    if os.environ.get('CI', None) is None:
        parser.error('Are you running this locally?')

    render_index(args)
