"""Query firestore for logs.

Query Google firestore associated with this project for stored Error logs,
filtered by email, a date or a specific id.
"""
import argparse
import shutil
from google.cloud import firestore
from datetime import datetime

DATABASE = firestore.Client()


class _StoreDateAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string):
        try:
            args = [int(x) for x in values.split('/')]
            val = int(datetime.timestamp(datetime(*args)) * 1000)
            setattr(namespace, self.dest, val)
        except (ValueError, IndexError):
            raise argparse.ArgumentError(
                self,
                "must be given with the pattern YYYY/MM/DD, can't " +
                'parse {val}'.format(val=values)
            )


def _print_item(item, full=False, len_mails=None, len_versions=None):
    data = item.to_dict()
    if data is None:
        print ('No item matching the query was found.')
        return
    f_string = None
    stack = data.get('stacktrace', 'NO STACKTRACE')
    doc_id = item.reference.id
    t = int(data['time']) / 1000.0
    dt = datetime.fromtimestamp(t).strftime('%Y-%m-%d %H:%M:%S')

    terminal_width, _ = shutil.get_terminal_size()

    if full:
        f_string = 'email: {mail}\nversion: {version}\ntime: {dt}\n{stack}'
    else:
        separators = {
            'd_m': ' : ',
            'm_v': ' @ ',
            'v_d': ' - ',
            'd_s': ' - ',
            'e': '...'
        }
        f_string = '''\
{{doc_id}}{d_m}{{mail:>{l_m}}}{m_v}{{version:>{l_v}}}{v_d}\
{{dt}}{d_s}{{stack:.{l_s}}}{e}\
'''
        len_stack = terminal_width - (
            len_mails +
            len_versions +
            len(doc_id) +
            len(dt) +
            len(''.join(separators.values()))
        )
        f_string = f_string.format(
            l_m=len_mails,
            l_v=len_versions,
            l_s=30 if len_stack < 10 else len_stack,
            **separators
        )

    print(f_string.format(
        doc_id=doc_id,
        mail=data.get('email', 'NO EMAIL'),
        dt=dt,
        stack=stack,
        version=data.get('version', 'NO V'),
    ))


def _get_snapshots(older_than=None, newer_than=None, email=None, **kwargs):
    mails = set()
    versions = set()
    docs_array = []

    docs_query = DATABASE.collection('logs').order_by('time')

    if email is not None:
        docs_query = docs_query.where('email', '==', email)

    if newer_than is not None:
        docs_query = docs_query.where('time', '>=', newer_than)

    if older_than is not None:
        docs_query = docs_query.where('time', '<=', older_than)

    for snap in docs_query.stream():
        try:
            mails.add(snap.get('email'))
        except KeyError:
            pass
        try:
            versions.add(snap.get('version'))
        except KeyError:
            pass
        docs_array.append(snap)

    max_mail = max([len(x) for x in mails])
    max_version = max([len(x) for x in versions])

    return docs_array, max_mail, max_version


def _get(doc_id=None, **kwargs):
    if doc_id is None:
        return _get_snapshots(**kwargs)
    return [DATABASE.document('logs/' + doc_id).get()], None, None


def _show_logs(doc_id=None, **kwargs):
    snaps, len_mails, len_versions = _get(doc_id=doc_id, **kwargs)
    for snap in snaps:
        _print_item(snap, doc_id is not None, len_mails, len_versions)


def _delete(**kwargs):
    snaps, _, _ = _get(**kwargs)
    for snap in snaps:
        snap.reference.delete()


def _main():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    parser_collection = parser.add_subparsers(help='Available commands')

    show_parser = parser_collection.add_parser('show', help='Retrieve logs')
    show_parser.set_defaults(func=_show_logs)

    main_group = show_parser.add_mutually_exclusive_group()
    dates_group = main_group.add_mutually_exclusive_group()
    dates_group.add_argument(
        '-o', '--older_than', type=str, action=_StoreDateAction,
        help='Query for logs older than (exclusive) YYYY/MM/DD'
    )
    dates_group.add_argument(
        '-n', '--newer_than', type=str, action=_StoreDateAction,
        help='Query for logs newer than (inclusive) YYYY/MM/DD'

    )
    main_group.add_argument(
        '-e', '--email', type=str,
        help='Query for logs for a specific email'
    )
    main_group.add_argument(
        '-i', '--id', type=str, dest='doc_id',
        help='Show full details of a specific log id'
    )

    delete_parser = parser_collection.add_parser('delete', help='Delete logs')
    delete_parser.set_defaults(func=_delete)
    delete_filter_group = delete_parser.add_mutually_exclusive_group(
        required=True
    )
    delete_filter_group.add_argument(
        '-o', '--older_than', type=str, action=_StoreDateAction,
        help='Delete logs older than (exclusive) YYYY/MM/DD'
    )
    delete_filter_group.add_argument(
        '-e', '--email', type=str,
        help='Delete logs for a specific email'
    )
    delete_filter_group.add_argument(
        '-i', '--id', type=str, dest='doc_id',
        help='Delete a specific log entry by id'
    )

    args = parser.parse_args()
    args.func(**(vars(args)))


if __name__ == '__main__':
    _main()
