# SheepSync tools

## Prerequisites

In a Python 3 environment:

    $ pip install -r requirements.txt

## Logs

Usage:

    $ export GOOGLE_APPLICATION_CREDENTIALS=<path/to/secret/key.json>
    $ python3 firestore/logs.py
    $ python3 firestore/logs.py -h
