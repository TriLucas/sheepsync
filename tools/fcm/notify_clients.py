from functools import wraps
import argparse
import json
import uuid

from firebase_admin import initialize_app
from firebase_admin.messaging import (Message, send, QuotaExceededError,
                                      SenderIdMismatchError,
                                      ThirdPartyAuthError, UnregisteredError)


initialize_app()


def topic(topic_name):

    def decorator_topic(func):
        @wraps(func)
        def wrapper_topic(production=False, *args, **kwargs):
            topic = topic_name + ('-dev' if not production else '')
            return func(*args, topic=topic, **kwargs)

        return wrapper_topic

    return decorator_topic


def _send_message(data, topic, verbose, **kwargs):
    if verbose:
        print('Sending {} to topic {}'.format(data, topic))
    new_message = Message(data=data, topic=topic)

    try:
        send(new_message)
    except (QuotaExceededError, SenderIdMismatchError, ThirdPartyAuthError,
            UnregisteredError) as e:
        print('Error sending notification')
        print(e)


@topic('update')
def notify_update(filename, **kwargs):
    data = json.load(open(filename, 'r'))
    _send_message({'new_version': data[0]['apkData']['versionName']}, **kwargs)


@topic('spawnServices')
def spawn_services(force, **kwargs):
    _send_message({'force': str(force)}, **kwargs)


@topic('requestConfig')
def request_config(unique, **kwargs):
    data = {}
    if unique:
        data['uuid'] = uuid.uuid4().hex[:16].upper()
    _send_message(data, **kwargs)


def build_parser():
    parser = argparse.ArgumentParser(description='Notify clients about events')
    parser.add_argument('-P', '--production', action='store_true',
                        default=False, help='Use development channel')
    parser.add_argument('-v', '--verbose', action='store_true', default=False)

    subparsers = parser.add_subparsers()
    v_parser = subparsers.add_parser('version',
                                     help='Notify about a new version')
    v_parser.set_defaults(func=notify_update)
    v_parser.add_argument('-f', '--filename', required=True)

    s_parser = subparsers.add_parser('spawn',
                                     help='Remotely fire services')
    s_parser.set_defaults(func=spawn_services)
    s_parser.add_argument('-f', '--force', required=False, action='store_true',
                          default=False)

    c_parser = subparsers.add_parser('config',
                                     help='Request client configurations.')
    c_parser.set_defaults(func=request_config)
    c_parser.add_argument('-u', '--unique', required=False,
                          action='store_true', default=False)

    return parser


if __name__ == '__main__':
    parser = build_parser()

    args = parser.parse_args()
    args.func(**(vars(parser.parse_args())))
